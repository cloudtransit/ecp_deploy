require 'securerandom'

# Basic class that holds the data for a kubernetes cluster
#
# Author:: Arne Salveter, Christian-Friedrich Stüben
class Cluster

  NAME_LENGTH = 5

  attr_accessor :name, :machines, :public_ipv4, :storage_cluster, :storage_cluster_network, :keypair, :secgrp_name, :open_ports
  attr_accessor :ssl_path, :ca_pem, :ca_key, :adm_pem, :adm_key, :volume_name, :master_count

  # Constructor needs a name for this cluster
  # @param A nameto describe the cluster.
  def initialize(name)
    @name = name
    @public_ipv4=''
    @storage_cluster=nil
    @storage_cluster_network=''
    @volume_name=''
    @keypair=''
    @machines = Array.new
    @secgrp_name = ''
  end

  # Generate new uid for machines
  def get_next_uid
    run = true
    tmp = SecureRandom::hex(NAME_LENGTH)
    while run
      contain = nil
      contain = @machines.select {|m| m.hostname == "#{tmp}"} unless @machines.nil?
      if contain.nil? || contain.empty?
        run = false
      else
        tmp = SecureRandom::hex(NAME_LENGTH)
      end
    end
    tmp
  end

  # Adds a machine to the cluster
  # @param machine Add this machine to this cluster.
  def addMachine(machine)
    machine.hostname = get_next_uid
    @machines.push(machine)
  end

  # Human readable status string
  def to_s
    "#{@name}@#{@public_ipv4} \n" + @machines.to_s.gsub(',', "\n")
  end

  def getmaster
    @machines.find {|m| m.role.downcase =="master"}
  end

  def has_machine_with_uid(uid)
    !(@machines.select {|m| m.uid==uid}.empty?)
  end

  # Gets array of all roles used in cluster
  # @return Array of roles in cluster
  def get_cluster_roles
    roles = []
    machines.each do |machine|
      roles.push(machine.role) unless roles.include?(machine.role)
    end
    roles
  end

  # Gets all machine of specific role
  # @param role Filtered role
  # @return Array of machines with role
  def get_machines_by_role(role)
    @machines.select{|m| m.role==role}
  end

  # Get machine by ip
  # @param ip ip address as String
  # @return machine or nil
  def get_machine_by_ip(ip)
    arr = @machines.select{|m|m.private_ipv4==ip}
    arr.nil? ? arr : @machines.select{|m|m.private_ipv4==ip}.first
  end

  # Removes a machine from cluster using machine-object or a machine-uid
  def rmmachine(var)
    if var.kind_of? Machine
      @machines.delete(var)
    else
      @machines.delete_if {|m| m.uid==var}
    end
  end

  # Removes machine identified by hostname
  def rm_machine_by_hostname(hostname)
    @machines.delete_if{ |m| m.hostname==hostname}
  end

  # Find and set initialized flag for machine with ip
  def set_initialized_by_ip(ip,flag=true)
    m = get_machine_by_ip(ip)
    m.initialized = flag unless m.nil?
  end

  # Returns array of initialized machines
  def select_intialized
    @machines.select{ |m| m.initialized}
  end

  def rmAllMachine
    @machines.clear
  end


end
