# Basic class that holds the data for a machine of
# a kubernetes cluster

# Author:: Arne Salveter, Christian-Friedrich Stüben, Thomas Finnern
class Machine

  attr_accessor :flavor, :image, :role, :hostname, :uid, :state, :provider,
                :storage_cluster, :private_ipv4, :public_ipv4, :public_dns_name,
                :last_state, :volume_id, :volume_size, :starting_time, :initialized

  # Constructor
	# @param provider A provider like aws or openStack.
	# @param image An operating system	.
	# @param flavor Descrips the hardware for the virtual machine.
	# @param role Like: Is this machine an master or a slave node.
  def initialize(provider, storage_cluster, image, flavor, role, hostname)
    @provider = provider
    @storage_cluster = storage_cluster
    @image = image
    @flavor = flavor
    @role = role
    @hostname = hostname
    @uid = ''
    @private_ipv4 = ''
    @public_ipv4 = ''
    @public_dns_name = ''
    @last_state = ''
    @volume_id = ''
    @volume_size = ''
		@starting_time = Time.now.to_i
    @initialized = false
  end


  # Human readable status of this machine
  def to_s
    "#{@role}:\n--#{@image}\n--#{@flavor}\nid:#{@uid}"
  end
end
