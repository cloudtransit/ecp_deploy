#!/usr/bin/env ruby

require_relative "storage"



# A special storage class for Flocker.

# Author:: Thomas Finnern
class FlockerStorage < Storage


  FLOCKER_TEMPLATE="../templates/flocker_storage_template.yaml"

  # Create the storage object
  #
  def initialize

  end


  def flocker_template
    File.join(File.dirname(File.expand_path(__FILE__)),FLOCKER_TEMPLATE)
  end


  def finalize(cluster)
    generateFlockerUserdata(cluster)
    Dir.chdir "flocker"
    command = "uft-flocker-install #{@flockerUserDataFile}"
    Cli::LOG.debug(self){"Running command: #{command}"}
    `#{command}`
    Cli::LOG.info(self){"Waiting 5 seconds..."}
    sleep(5)
    command = "uft-flocker-config #{@flockerUserDataFile}"
    Cli::LOG.debug(self){"Running command: #{command}"}
    `#{command}`
    Cli::LOG.info(self){"Waiting 5 seconds..."}
    sleep(5)
    command = "uft-flocker-plugin-install #{@flockerUserDataFile}"
    Cli::LOG.debug(self){"Running command: #{command}"}
    `#{command}`
    Cli::LOG.info(self){"Waiting 5 seconds..."}
    sleep(5)
    Dir.chdir ".."
  end

  def generate(cluster)
    #Nothing to do
  end


  # Add the cluster nodes to the storage cluster
   #
   # @param cluster A running cluster
   def generateFlockerUserdata(cluster)
       @name=cluster.name
       @ssl_path = cluster.ssl_path
       count = 1
       cluster.machines.each do |machine|
         if count == 2
           @node1_public_ipv4 = machine.public_ipv4
           @node1_private_ipv4 = machine.private_ipv4
           @dns_name_master = machine.public_dns_name
         elsif count == 3
           @node2_public_ipv4 = machine.public_ipv4
           @node2_private_ipv4 = machine.private_ipv4
         elsif count == 4
           @node3_public_ipv4 = machine.public_ipv4
           @node3_private_ipv4 = machine.private_ipv4
         end
         count += 1
       end
       @flockerUserData = File.read(flocker_template).gsub("<node1_public_ipv4>",@node1_public_ipv4.to_s).gsub("<node1_private_ipv4>",@node1_private_ipv4.to_s).gsub("<dns_name_master>",@dns_name_master.to_s).gsub("<node2_public_ipv4>",@node2_public_ipv4.to_s).gsub("<node2_private_ipv4>",@node2_private_ipv4.to_s).gsub("<node3_public_ipv4>",@node3_public_ipv4.to_s).gsub("<node3_private_ipv4>",@node3_private_ipv4.to_s).gsub("<name>",@name.to_s)
       command = "mkdir -p flocker"
       Cli::LOG.debug(self){"Running command: #{command}"}
       `#{command}`
       Cli::LOG.info(self){"Waiting 5 seconds..."}
       sleep(5)
       Dir.chdir "flocker"
       @flockerUserDataFile="flocker#{@name.to_s}.yml"
       File.open(@flockerUserDataFile, 'w') { |file| file.write(@flockerUserData) }
       Dir.chdir ".."
   end
   
  def generateSSHconfig(cluster)
    #Nothing to do
  end


end