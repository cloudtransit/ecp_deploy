#!/usr/bin/env ruby

require_relative "storage"



# A special storage class for Cephfs.

# Author:: Thomas Finnern
class CephfsStorage < Storage

  # Create the storage object
  #
  def initialize
    
  end
  
  
  def finalize(cluster)
    addClusterNodesToHostfile(cluster)
    addClusterNodes(cluster)
  end
  
  
  # Create the storage cluster
     #
     # @param cluster 
     def generate(cluster)
       time_to_wait = 5
       tmp_exe = "sudo rm ceph.conf"    
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       Cli::LOG.info(self){"Deleted old config files"}
       
       Cli::LOG.info(self){"Creating a new storage cluster with the 'ceph-deploy' command"} 
       tmp_exe = "ceph-deploy new mon1"    
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Writing storage config file..."}
       tmp_exe = "echo \"public network = #{cluster.storage_cluster_network} \nosd pool default size = 2\" >> ceph.conf"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       nodes = 'deployer '
       cluster.machines.each do |machine|
         if machine.role == "mon" || machine.role == "osd"
           entry = "#{machine.hostname} "
           nodes << entry
         end
       end
       
       Cli::LOG.info(self){"Installing the storage software on all storage nodes..."}
       tmp_exe = "ceph-deploy install #{nodes}"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Deploying the monitor node..."}
       tmp_exe = "ceph-deploy mon create-initial"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Creating keyrings..."}
       tmp_exe = "ceph-deploy gatherkeys mon1"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Deleting the partition tables on all nodes with the zap option..."}
       entries = ''
       cluster.machines.each do |machine|
         if machine.role == "osd"
          entry = "#{machine.hostname}:/dev/#{cluster.volume_name} "
          entries << entry
        end
       end
       tmp_exe = "ceph-deploy disk zap #{entries}"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Preparing all osd nodes..."}
       tmp_exe = "ceph-deploy osd prepare #{entries}"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Activating the osd's..."}
       osds_to_activate = ''
          cluster.machines.each do |machine|
            if machine.role == "osd"
             entry = "#{machine.hostname}:/dev/#{cluster.volume_name}1 "
              osds_to_activate << entry
           end
          end
          tmp_exe = "ceph-deploy osd activate #{osds_to_activate}"
          Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
          tmp_out = `#{tmp_exe}`
       
       Cli::LOG.info(self){"Deploying the management key to all associated nodes..."}   
       tmp_exe = "ceph-deploy admin deployer"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
       
       cluster.machines.each do |machine|
         if machine.role == "mon" || machine.role == "osd"    
           tmp_exe = "ceph-deploy admin #{machine.hostname}"
           Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
           tmp_out = `#{tmp_exe}`
           Cli::LOG.info(self){"Waiting #{time_to_wait} seconds before the next step..."}
           sleep(time_to_wait)
         end
       end
         
       Cli::LOG.info(self){"Changing the permission of the key file by running the admin command on all nodes..."}  
       tmp_exe = "sudo chmod 644 /etc/ceph/ceph.client.admin.keyring"
       Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
       tmp_out = `#{tmp_exe}`
         
       cluster.machines.each do |machine|
          if machine.role == "mon" || machine.role == "osd"    
            sshSend(machine.hostname,tmp_exe)
          end
       end
     end
     
     
  # Add the cluster nodes to the storage cluster
   #
   # @param cluster A running cluster
   def addClusterNodes(cluster)
     entries = ''
     disk = 'missing'
     time_to_wait = 10
     cluster.machines.each do |machine|
       if !(Storage.getClusterRoles.include? machine.role)
         Cli::LOG.info("Adding cluster nodes to the storage cluster...")
         command = "sudo modprobe rbd"
         sshSend(machine.hostname,command)
         command = "sudo rbd"
         sshSend(machine.hostname,command)
         command = "sudo chown core /etc/ceph"
         sshSend(machine.hostname,command)
         tmp_exe = "scp ceph.conf core@#{machine.hostname}:/etc/ceph"
         Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
         tmp_out = `#{tmp_exe}`
         Cli::LOG.info("Waiting for #{time_to_wait} seconds...")
         sleep(time_to_wait)
         tmp_exe = "scp ceph.client.admin.keyring core@#{machine.hostname}:/etc/ceph"
         Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
         tmp_out = `#{tmp_exe}`
         Cli::LOG.info("Waiting for #{time_to_wait} seconds...")
         sleep(time_to_wait)
         if disk == 'missing'
            command = "sudo rbd create disk --size 1000"
            sshSend(machine.hostname,command)
         end
         command = "sudo rbd feature disable disk exclusive-lock object-map fast-diff deep-flatten"
         sshSend(machine.hostname,command)
         Cli::LOG.info(self){"Waiting #{time_to_wait} seconds before the next step..."}
         sleep(time_to_wait)
         command = "sudo rbd map disk"
         sshSend(machine.hostname,command)
         command = "sudo mkdir -p /mnt/disk"
         sshSend(machine.hostname,command)
         if disk == 'missing'
            command = "sudo mkfs.xfs /dev/rbd0"
            sshSend(machine.hostname,command)
            disk = 'build'
         end   
         command = "sudo mount /dev/rbd0 /mnt/disk"
         sshSend(machine.hostname,command)
       end
     end
   end
   

  # Add cluster nodes to the /etc/hosts files
     #
     # @param cluster 
    def addClusterNodesToHostfile(cluster)
      Cli::LOG.info(self){"Adding cluster nodes to the /etc/hosts file..."}
      entries = ''
      cluster.machines.each do |machine|
        if !(Storage.getClusterRoles.include? machine.role)
        entries << getHostFileEntry(cluster,machine.uid)
        end
      end
      tmp_exe = "echo \"#{entries}\" >> /etc/hosts"
      Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
      tmp_out = `#{tmp_exe}`
    end
  
end