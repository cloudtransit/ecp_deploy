#!/usr/bin/env ruby

# Basic storage class that implements steps needed to deploy
# a storage cluster.

# Author:: Thomas Finnern

class Storage


  def initialize

  end
  
 # Prepare the storage cluster
   #
   # @param cluster 
  def prepare(cluster)
    generateSSHconfig(cluster)
    setHosts(cluster)
    setHostnames(cluster)
  end
  
  # Edit ssh config
   #
   # @param cluster 
  def generateSSHconfig(cluster)
    Cli::LOG.info(self){"Writing the hostnames into the ssh config file..."}
    entries = "Host deployer \n Hostname deployer \n User ubuntu \n IdentityFile ~/.ssh/#{cluster.keypair}.pem \n StrictHostKeyChecking no \n UserKnownHostsFile=/dev/null \n\n"
    cluster.machines.each do |machine|
      if machine.role == "mon" || machine.role == "osd" || machine.role == "storage"   
        entry = "Host #{machine.hostname} \n Hostname #{machine.hostname} \n User ubuntu \n IdentityFile ~/.ssh/#{cluster.keypair}.pem \n StrictHostKeyChecking no \n UserKnownHostsFile=/dev/null \n\n"
        entries << entry
      else
        entry = "Host #{machine.hostname} \n Hostname #{machine.hostname} \n User core \n IdentityFile ~/.ssh/#{cluster.keypair}.pem \n StrictHostKeyChecking no \n UserKnownHostsFile=/dev/null \n\n"
        entries << entry
      end
    end
    tmp_exe = "sudo truncate -s0 ~/.ssh/config"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    tmp_exe = "echo \"#{entries}\" >> ~/.ssh/config"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
  end 
  

  # Edit hosts config
   #
   # @param cluster 
  def setHosts(cluster)
    Cli::LOG.info(self){"Writing host ip and hostname into /etc/hosts..."}
    entries = '127.0.0.1 \t deployer \n'
    cluster.machines.each do |machine|
      if machine.role == "mon" || machine.role == "osd" || machine.role == "storage"
        entries << getHostFileEntry(cluster,machine.uid)
      end
    end
    tmp_exe = "sudo truncate -s0 /etc/hosts"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    tmp_exe = "echo \"#{entries}\" >> /etc/hosts"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    entries = ''
   end
   
   
  # Set the hostnames
   #
   # @param cluster 
   def setHostnames(cluster)
     Cli::LOG.info(self){"Changing the machine hostnames..."}
     cluster.machines.each do |machine|
       if machine.role == "mon" || machine.role == "osd"
         command = "sudo hostnamectl set-hostname #{machine.hostname}"    
         sshSend(machine.hostname,command)
         command = "sudo systemctl restart systemd-hostnamed"    
         sshSend(machine.hostname,command)
         machine_hostname = machine.hostname   
         entries = ''
         cluster.machines.each do |machine|
           if machine.role == "mon" || machine.role == "osd"
             entry = "#{machine.private_ipv4} #{machine.hostname} \n"
             entries << entry              
           end
         end
         command = "sudo echo \"#{entries}\" >> /etc/hosts"
         sshSend(machine_hostname,command)
       end
     end
   end
   
 
  # Add cluster nodes to the /etc/hosts files
     #
     # @param cluster 
    def addClusterNodesToHostfile(cluster)
      Cli::LOG.info(self){"Adding cluster nodes to the /etc/hosts file..."}
      entries = ''
      cluster.machines.each do |machine|
        if !(Storage.getClusterRoles.include? machine.role)
          entries << getHostFileEntry(cluster,machine.uid)
        end
      end
      tmp_exe = "echo \"#{entries}\" >> /etc/hosts"
      Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
      tmp_out = `#{tmp_exe}`
    end
   
   
  # Create /etc/hosts
   def getHostFileEntry(cluster,uid)
     cluster.machines.each do |machine|
       if machine.uid == uid 
         return "#{machine.private_ipv4} \t #{machine.hostname} \n"
       end
     end
   end
   
   
  # SSH send command
   #
   # @param host is the machine hostname 
   # @param cmd is the command to execute
  def sshSend(host,cmd)
      tmp_exe = "ssh #{host} '#{cmd}'"
      Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
      `#{tmp_exe}`
   end

 # static: description of all storage roles
  def self.getClusterRoles
    return ['mon', 'osd', 'storage', 'flocker', 'cephfs', 'glusterfs', 'flocker']
  end


end
