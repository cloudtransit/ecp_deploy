#!/usr/bin/env ruby

require_relative "storage"



# A special storage class for Glusterfs.

# Author:: Thomas Finnern
class GlusterfsStorage < Storage

  # Create the storage object
  #
  def initialize

  end

  # Create the storage cluster
     #
     # @param cluster
     def generate(cluster)    
        Cli::LOG.info(self){"Building the storage cluster"}
        create_volume = ''
        hostname = ''
        sleep_gluster_volume_start = 10
        #todo - every step should have a check procedure intead of a timer
        mount_path = "/mnt/gluster"
        #todo - the mount_path should be set in the config file
        volume_name = "vol1"
        #todo - the volume_name should be set in the config file
        cluster.machines.each do |machine|
          if machine.role.downcase =="storage"
            command = "sudo umount /dev/#{cluster.volume_name}"
            sshSend(machine.hostname,command,cluster.keypair)
            wait
            command = "sudo mkfs.ext4 /dev/#{cluster.volume_name}"
            sshSend(machine.hostname,command,cluster.keypair)
            wait
            command = "sudo mkdir #{mount_path} -p"
            sshSend(machine.hostname,command,cluster.keypair)
            wait
            command = "sudo mount /dev/#{cluster.volume_name} #{mount_path}"
            sshSend(machine.hostname,command,cluster.keypair)
            wait
            hostname = machine.hostname
            cluster.machines.each do |machine|
            if machine.role.downcase =="storage"
              if hostname != machine.hostname
                Cli::LOG.info(self){"Adding storage machine with private ip #{machine.private_ipv4} to the cluster..."}
                command = "sudo gluster peer probe #{machine.private_ipv4}"
                sshSend(hostname,command,cluster.keypair)
              end
            end
          end
        end
      end
      cluster.machines.each do |machine|
        if machine.role.downcase =="storage"
          create_volume_entry = machine.private_ipv4 + ":#{mount_path} "
          create_volume << create_volume_entry
        end
      end
      command = "sudo gluster volume create #{volume_name} replica 2 transport tcp #{create_volume}force"
      sshSend(hostname,command,cluster.keypair)
      command = "sudo gluster volume start #{volume_name}"
      sshSend(hostname,command,cluster.keypair)
      Cli::LOG.info(self){"Started storage volume. Waiting #{sleep_gluster_volume_start} seconds before mounting a directory..."}
      sleep(sleep_gluster_volume_start)
      cluster.machines.each do |machine|
    end
  end

  def finalize(cluster)
    #Nothing to do
  end
  
  
  def generateSSHconfig(cluster)
    #Nothing to do
  end
  
  
  def wait
    seconds = 5
    Cli::LOG.info("Waiting for #{seconds} seconds...")
    sleep(seconds)
  end

  
  # SSH send command
  #
  # @param host is the machine hostname
  # @param cmd is the command to execute
  # @param keypair is the .pem keypair name
  def sshSend(host,cmd,keypair)
    tmp_exe = "ssh -i /home/ubuntu/.ssh/#{keypair}.pem #{host} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \"#{cmd}\""
    #todo - the path to the keypair should be set in the config file
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    return `#{tmp_exe}`
  end

end