#!/usr/bin/env ruby

require_relative "provider"

require 'yaml'
require 'json'

# A special provider class for AWS (aws-cli based).

# Author:: Arne Salveter, Christian-Friedrich St�ben
class AwsProvider < Provider

  attr_accessor :credentials, :provideruid, :master_ip


  # Create the provider object
  #
  # @param credentials Given AWS credentials
  def initialize(credentials)
    @credentials = credentials
    @provideruid = "aws"

    setupRegion

  end

  def setupRegion

    exec = "aws configure set default.region #{@credentials.region}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
    unless $?.exitstatus==0
      Cli::LOG.fatal(self) { "AWSCLI is not installed" }
      exit(false)
    end

  end


  def getStartingTime(machine)
    Cli::LOG.debug(self) {"Set aws start-time"}
    exec = "aws ec2 describe-instances --instance-ids #{machine.uid} --output json"
    Cli::LOG.debug(self) {"Executing command: #{exec}"}
    tmp_out = `#{exec}`
    begin
      date =  JSON.parse(tmp_out)["Reservations"][0]["Instances"][0]["LaunchTime"]

      year = date.split('T')[0].split('-')[0]
      month = date.split('T')[0].split('-')[1]
      day = date.split('T')[0].split('-')[2]
      hour = date.split('T')[1].split('.')[0].split(':')[0]
      minute =  date.split('T')[1].split('.')[0].split(':')[1]
      sec = date.split('T')[1].split('.')[0].split(':')[2]
      plusHour = "00:00"

      newDate = Time.new(year, month, day, hour, minute, sec, "+" + plusHour)
      Cli::LOG.debug(self) {"Set start-time for machine-#{machine.uid} to #{newDate.to_i}"}
      return newDate.to_i

    rescue Exception => e
      time = Time.now().to_i
      Cli::LOG.debug(self) {"Error: #{e}"}
      Cli::LOG.debug(self) {"Set start-time for machine-#{machine.uid} to current system time #{time}"}
      return time
    end
  end


  def generate_server(machine,sec_group,datafile)
    tmp_exec = "aws ec2 run-instances --image-id #{machine.image} --instance-type #{machine.flavor} --output text --security-groups #{sec_group} --key-name #{@credentials.key_pair} --user-data file://#{datafile}"
    Cli::LOG.debug(self) { "Running command: #{tmp_exec}" }
    tmp_out = `#{tmp_exec}`
    machine.uid = tmp_out.split(" ")[8].gsub(' ', '')

    Cli::LOG.info(self) { "Setting name tag to: #{name}" }
    out = "aws ec2 create-tags --output text --resources #{machine.uid} --tags Key=Name,Value=#{machine.hostname}"
    Cli::LOG.debug(self) { "Running command: #{out}" }
    `#{out}`
  end

  # Create the Master machine
  #
  # @param machine A object, that describe a cluster node
  # @param sec_group The security-group for the machine
  def generateMaster(machine, sec_group)
    super(machine,sec_group)
    name=@name.gsub('', '')+"Master"

#    Cli::LOG.info(self) { "Setting name tag to: #{name}" }
#    out = "aws ec2 create-tags --output text --resources #{@master_uid} --tags Key=Name,Value=#{name}"
#    Cli::LOG.debug(self) { "Running command: #{out}" }
#    `#{out}`
  end

  def get_machine_ip machine
    #TODO: Methode von anderer Stelle aus aufrufen, wird mehrfach verwendet, siehe assocfloatingip
    Cli::LOG.debug(self){"Getting IP address of machine: #{machine.uid}"}
    tmp_out = "aws ec2 describe-instances --instance-ids #{machine.uid}  --output text"
    out = `#{tmp_out}`.firstlineoccured('NETWORKINTERFACES').split(' ')[5].gsub(' ','')
    Cli::LOG.debug(self){"IP address is: #{out}"}
    out
  end

  # Create the slave machine
  #
  # @param machine A object, that describe a cluster node
  # @param sec_group The security-group for the machine
  def generateSlave(machine, sec_group)
    super(machine,sec_group)
  end

  # Create the storage machine
  #
  # @param machine A object, that describe a storage node
  # @param sec_group The security-group for the machine
  def generateStorage(machine, sec_group)
    tmp_exe = "aws ec2 run-instances --image-id #{machine.image} --output text --region eu-central-1 --key-name #{@credentials.key_pair} --instance-type #{machine.flavor} --security-groups #{sec_group} --iam-instance-profile Name=allowSendingSsmCommands --user-data file://#{@storageUserDataFile}"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    machine.uid = tmp_out.split(" ")[8]
    machine.private_ipv4 = tmp_out.split(" ")[13]
    Cli::LOG.info(self){"Generated instance #{machine.uid} with private ip #{machine.private_ipv4}"}
    generateVolume(machine)
    @keypair = @credentials.key_pair
    @network_CIDR = @credentials.network_CIDR
    @volume_name = @credentials.volume_name
    @storage_ip = machine.private_ipv4
  end


  # Create a storage volume
  #
  # @param machine A object, that describe a storage node
  def generateVolume(machine)
    machine.volume_size = 8
    tmp_exe = "aws ec2 create-volume --size #{machine.volume_size} --region eu-central-1 --availability-zone eu-central-1b --volume-type gp2"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    if tmp_out.split(" ")[6]["vol"] #Necessary, because AWS often changes the position of the volume id
      machine.volume_id = tmp_out.split(" ")[6]
    elsif "vol".in? tmp_out.split(" ")[7]["vol"]
      machine.volume_id = tmp_out.split(" ")[7]
    else
      machine.volume_id = tmp_out.split(" ")[8]
    end
    machine.volume_id = machine.volume_id.gsub('"','').gsub(",","")
    Cli::LOG.info(self){"Generated volume with #{machine.volume_size} GB (#{machine.volume_id})"}
  end


  # Attach the storage volumes
  #
  # @param cluster A object, that describe a storage node
  def attachStorageVolumes(cluster)
    time_to_wait = 5
    cluster.machines.each do |machine|
      if machine.role == "osd" || machine.role == "storage"
        command = "aws ec2 attach-volume --volume-id #{machine.volume_id} --instance-id #{machine.uid} --device /dev/sdf"
        Cli::LOG.debug(self){"Running command: #{command}"}
        `#{command}`
        Cli::LOG.info(self){"Attached volume with #{machine.volume_size} GB (#{machine.volume_id}) to #{machine.role} with public ip #{machine.public_ipv4}"}
      end
    end
    Cli::LOG.info("Waiiting #{time_to_wait} seconds...")
    sleep(time_to_wait)
    configureStorageVolumes(cluster)
  end


  # Prepare the osd storage volumes
  #
  # @param cluster
  def configureStorageVolumes(cluster)
    cluster.machines.each do |machine|
      if machine.role == "osd"
        Cli::LOG.info(self){"Preparing the volume (#{machine.volume_id}) from #{machine.role} with public ip #{machine.public_ipv4}"}
        command = "sudo parted -s /dev/#{@credentials.volume_name} mklabel gpt mkpart primary xfs 0% 100%"
        sshSend(machine.hostname,command)
        command = "sudo mkfs.xfs -f /dev/#{@credentials.volume_name}"
        sshSend(machine.hostname,command)
      end
    end
  end


  # SSH send command
  #
  # @param host is the machine hostname
  # @param cmd is the command to execute
  def sshSend(host,cmd)
    command = "ssh #{host} '#{cmd}'"
    Cli::LOG.debug(self){"Running command: #{command}"}
    `#{command}`
  end


  # Check machine system state
  #
  # @param machine A object, that describe a storage node
  def checkMachineSystemState(cluster,uid,role)
    seconds_per_check = 30
    max_checks = 15
    Cli::LOG.info("Checking machine system state...")
    cluster.machines.each do |machine|
      if machine.uid == uid
        checks = 1
        while machine.last_state != "ok"  do
          tmp_exe="aws ec2 describe-instance-status --query \"InstanceStatuses[?InstanceId=='#{machine.uid}'].InstanceStatus.Status\" --output text"
          Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
          tmp_out = `#{tmp_exe}`
          machine.last_state = tmp_out.strip
          if machine.last_state != "ok"
            Cli::LOG.info("Machine with uid #{machine.uid} has not completely started. Checking state in #{seconds_per_check} seconds...")
            sleep(seconds_per_check)
          end
          if checks > max_checks
            Cli::LOG.debug(self){"Machine could not be started in the given time. Aborting..."}
            return false
          end
          checks += 1
        end
        Cli::LOG.info("Machine with uid #{machine.uid} has completely started")
        tmp_exe="aws ec2 describe-instances   --query \"Reservations[*].Instances[?InstanceId=='#{uid}'].PublicIpAddress\"   --output=text"
        Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
        tmp_out = `#{tmp_exe}`
        machine.public_ipv4 = tmp_out.strip
        tmp_exe="aws ec2 describe-instances   --query \"Reservations[*].Instances[?InstanceId=='#{uid}'].PublicDnsName\"   --output=text"
        Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
        tmp_out = `#{tmp_exe}`
        machine.public_dns_name = tmp_out.strip
      end
    end
  end
  



  # Create the security-group
  #
  # @param cluster A running cluster
  def generatesecuritygrp(cluster)
    #secname = "sg_"+@name.gsub(" ", "")
    secname = cluster.get_next_uid

    Cli::LOG.info(self) { "Generated security-group: #{secname}" }
    exec="aws ec2 create-security-group --group-name  #{secname} --output text --description \"#{secname} security group\"  "
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    out=`#{exec}`
    cluster.secgrp_name = secname
  end


  # Add security-ruls
  #
  # @param cluster A running cluster
  def generatesecurityrules(cluster)


    exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol tcp --port  0-65535 --source-group #{cluster.secgrp_name}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`

    exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol udp --port  0-65535 --source-group #{cluster.secgrp_name}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`

    exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol icmp --port  0-65535 --source-group #{cluster.secgrp_name}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`

    exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol tcp --port 22 --cidr 0.0.0.0/0"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`

    exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol tcp --port 6443 --cidr 0.0.0.0/0"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
=begin
    ports = cluster.open_ports + NEEDED_PORTS
    Cli::LOG.info(self) { "Start: adding security rules" }
    Cli::LOG.info(self) { "Enable ping for group: #{cluster.secgrp_name}" }
    ports.each do |port|
      Cli::LOG.info(self) { "open port #{port}" }
      exec = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol tcp --port #{port} --cidr 0.0.0.0/0"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`
    end
    Cli::LOG.info(self) { "done: Adding security rules" }
=end
  end
  
  
    # Add a security rule for the storage cluster
    #
    # @param cluster A running cluster
    def addstoragesecurityrules(cluster)
      addsecurityrule("111","tcp",cluster)
      addsecurityrule("111","udp",cluster)
      addsecurityrule("2049","udp",cluster)
      addsecurityrule("24007","tcp",cluster)
      addsecurityrule("24008","tcp",cluster)
      addsecurityrule("38465","tcp",cluster)
      addsecurityrule("38466","tcp",cluster)
      addsecurityrule("38467","tcp",cluster)
      addsecurityrule("49152","tcp",cluster)
      addsecurityrule("49153","tcp",cluster)
      from_port = "2003"
      to_port = "7300"
      protocol = "tcp"
      Cli::LOG.info(self){"Add storage security rule"}
      command = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --ip-permissions '[{\"IpProtocol\": \"#{protocol}\", \"FromPort\": #{from_port}, \"ToPort\": #{to_port}, \"IpRanges\": [{\"CidrIp\": \"#{@credentials.network_CIDR}\"}]}]'"
      Cli::LOG.debug(self){"Running command: #{command}"}
      `#{command}`
    end
       
    def addsecurityrule(port,protocol,cluster)
      command = "aws ec2 authorize-security-group-ingress --group-name #{cluster.secgrp_name} --output text --protocol #{protocol} --port #{port} --cidr #{@credentials.network_CIDR}"
      Cli::LOG.debug(self){"Running command: #{command}"}
      `#{command}`
    end




  # Delete a security-group
  #
  # @param cluster A running cluster
  def deletesecuritygrp(cluster)

    time = 8
    run = false

    while !run and 0 < time do

      exec="aws ec2 describe-instances --output json --query #{"Reservations[].Instances[].[State.Name]"}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      state = `#{exec}`
      Cli::LOG.debug(self) { "Instances: #{state}" }

      sleep(8)
      string = "#{state}"

      unless (string.to_s).include? 'shutting-down'
        run = true
        Cli::LOG.info(self) { "All instances are down" }
        Cli::LOG.info(self) { "delete securitygrp: #{cluster.secgrp_name}" }
        exec="aws ec2 delete-security-group --group-name #{cluster.secgrp_name}  --output text"
        Cli::LOG.debug(self) { "Running command: #{exec}" }
        `#{exec}`
      end

      Cli::LOG.debug(self) { "Run-Variable is: #{run}" }
      Cli::LOG.debug(self) { "Timer: #{time}" }

      time = time - 1
    end

    run
  end


  # Currently not necessary
  def generateFloatingIP
    Cli::LOG.info(self) { "FloatingIPs are not necessary for aws" }
  end

  # Currently not necessary
  def assocFloatingIP
    Cli::LOG.info(self) { "Set masterIP" }
    tmp_out = `aws ec2 describe-instances --instance-ids #{@master_uid}  --output text`
    @master_ip = tmp_out.split(" ")[62]
    puts "MasterIP #{@master_ip}"
  end

  def deletemachine(machine)
    puts "delete machine: #{machine.uid}"
    Cli::LOG.info(self) { "delete machine: #{machine.uid}" }
    tmp_out = "aws ec2 terminate-instances --instance-ids #{machine.uid}  --output text"
    Cli::LOG.debug(self) { "Running command: #{tmp_out}" }
    `#{tmp_out}`
  end

  # Currently not necessary
  def remove_floating_ip(cluster)
  end

  # Generates cloud-init script for master server using a given template and the generated main certificate.
  def generateMasterUserdata(master_count,  hostname)
    generate_master_userdata(master_count, hostname,'$public_ipv4')
  end

  # Generates master-update-userdata
  def masterupdateuserdata(etcd_name, etcd_ic_list)
    super(etcd_name,etcd_ic_list,'$public_ipv4')
  end

  # deprecated, use get_machine_ip instead
  def getIP(hostname,uid)
    tmp_exe="aws ec2 describe-instances   --query \"Reservations[*].Instances[?InstanceId=='#{uid}'].PrivateIpAddress\"   --output=text"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    return tmp_out.strip
  end

  # A last order for a nice success message.
  #
  # could overwriting in a special provider
  def lastOrder()
    puts "job is done!"
  end


end
