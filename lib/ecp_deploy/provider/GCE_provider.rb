#!/usr/bin/env ruby
require_relative "provider"
require 'yaml'

# Google Compute Engine specific provider class that implements missing functions from provider class
# that are needed to deploy a kubernetes cluster using the gcloud command line interface.

class GceProvider < Provider


  # Create the provider object
  #
  # @param credentials given GCE credentials
  def initialize(credentials)
    @credentials = credentials
    @provideruid = "gce"
    setupRegion
  end

  def setupRegion
    exec = "gcloud config set compute/region #{@credentials.region}"
    Cli::LOG.debug(self) {"#{exec}"}
    `#{exec}`
  end


  def getStartingTime(machine)
    Cli::LOG.debug(self){"Getting IP address of machine: #{machine.uid}"}
    tmp_out = `gcloud compute instances describe  #{machine.uid} --zone #{@credentials.region} --format yaml`
    tmp =  YAML.load(tmp_out)
    time=tmp['creationTimestamp']
    Cli::LOG.debug(self){"Starting time: #{time}"}
    return time
  end


  def generate_server(machine,sec_group,datafile)
    name = "server" + Cluster.getInstanceID.to_s
    machine.uid = name
    exec = "gcloud compute instances create -q #{name} --zone #{@credentials.region} --tags master --machine-type #{machine.flavor} --image  #{machine.image} --image-project #{@credentials.image_project} --metadata-from-file startup-script=#{datafile} --format yaml"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    out=`#{exec}`

    machine.uid
  end


  # Create the Master machine
  #
  # @param machine A object, that describe a cluster node
  # @param sec_group The security-group for the machine
  def generateMaster(machine, sec_group)
    super(machine,sec_group)
    name=@name.gsub('', '')+"Master"
    Cli::LOG.info(self){"generate master"}
    exec = "gcloud compute instances add-metadata  #{machine.uid} --zone #{@credentials.region} --metadata Name=#{name}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    Cli::LOG.info(self){"Generated master with uid: #{machine.uid}"}
    out=`#{exec}`
  end


  def get_machine_ip machine
    Cli::LOG.debug(self){"Getting IP address of machine: #{machine.uid}"}
    tmp_out = `gcloud compute instances describe  #{machine.uid} --zone #{@credentials.region} --format yaml`
    tmp =  YAML.load(tmp_out)
    ip=tmp["networkInterfaces"][0]["accessConfigs"][0]["natIP"]
    Cli::LOG.debug(self){"IP address is: #{ip}"}
    ip
  end




  def getIP (hostname,uid)
    Cli::LOG.debug(self){"Getting IP address of machine: #{uid}"}
    tmp_out = `gcloud compute instances describe  #{uid} --zone #{@credentials.region} --format yaml`
    tmp =  YAML.load(tmp_out)
    ip=tmp["networkInterfaces"][0]["accessConfigs"][0]["natIP"]
    Cli::LOG.debug(self){"IP address is: #{ip}"}
    return ip
  end




  # Create the slave machine
  #
  # @param machine A object, that describe a cluster node
  # @param sec_group The security-group for the machine
  def generateSlave(machine, sec_group)
    super(machine,sec_group)
  end



  # Create the security-group
  #
  # @param cluster A running cluster
  def generatesecuritygrp(cluster)
    secname = "securitygroup-" + cluster.name.downcase.to_s + "-" + Cluster.getInstanceID.to_s
    Cli::LOG.info(self){"Generat security-group: #{secname}"}
    exec="gcloud compute networks create #{secname} --mode=auto"
    Cli::LOG.debug(self){"Running command: #{exec}"}
    out=`#{exec}`
    cluster.secgrp_name = secname
  end

  # Add security-ruls
  #
  # @param cluster A running cluster
  def generatesecurityrules(cluster)
    name = "firewall-" + cluster.secgrp_name.downcase.to_s
    ports = cluster.open_ports + NEEDED_PORTS
    Cli::LOG.info(self) { "Start: adding security rules" }
    tcp=""
    udp=""
    ports.each do |port|
      Cli::LOG.info(self) { "open port #{port}" }
      tcp = tcp + "tcp:" + port.to_s + ","
      udp = udp + "udp:" + port.to_s + ","
    end
    exec = "gcloud compute firewall-rules create #{name} --network #{cluster.secgrp_name} --allow #{tcp}#{udp}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
    Cli::LOG.info(self) { "done: Adding security rules" }
  end

  # Delete a security-group
  #
  # @param cluster A running cluster
  def deletesecuritygrp(cluster)
    firewallName = "firewall-" + cluster.secgrp_name.to_s
    Cli::LOG.info(self){"delete firewall: #{firewallName}"}
    exec="gcloud compute firewall-rules delete -q #{firewallName} --format yaml"
    Cli::LOG.debug(self){"Running command: #{exec}"}
    `#{exec}`
    Cli::LOG.info(self){"delete securitygrp: #{cluster.secgrp_name}"}
    exec="gcloud compute networks delete -q #{cluster.secgrp_name} --format yaml"
    Cli::LOG.debug(self){"Running command: #{exec}"}
    `#{exec}`
  end

  # Currently not necessary
  def generateFloatingIP
    Cli::LOG.info(self){"FloatingIPs are not necessary for gce"}
  end

  # Currently not necessary
  def assocFloatingIP
    Cli::LOG.info(self){"Set masterIP"}
    tmp_out = `gcloud compute instances describe  #{@master_uid} --zone #{@credentials.region} --format yaml`
    tmp =  YAML.load(tmp_out)
    @master_ip = tmp["networkInterfaces"][0]["accessConfigs"][0]["natIP"]
    puts "MasterIP #{@master_ip}"
  end

  def deletemachine(machine)
    Cli::LOG.info(self){"delete machine: #{machine.uid}"}
    tmp_out = "gcloud compute instances delete -q #{machine.uid} --zone #{@credentials.region} --format yaml"
    Cli::LOG.debug(self){"Running command: #{tmp_out}"}
    `#{tmp_out}`
  end

  # Currently not necessary
  def remove_floating_ip(cluster)
  end

  # Generates cloud-init script for master server using a given template and the generated main certificate.
  def generateMasterUserdata(master_count, hostname)
    generate_master_userdata(master_count,hostname,'$public_ipv4')
  end

  # Generates master-update-userdata
  def masterupdateuserdata(etcd_name, etcd_ic_list)
    super(etcd_name,etcd_ic_list,'$public_ipv4')
  end



  # A last order for a nice success message.
  #
  # could overwriting in a special provider
  def lastOrder()
    Cli::LOG.debug(self) {"remove gce-service-account"}
    exec = "gcloud auth revoke #{@service_account}"
    Cli::LOG.debug(self) {"#{exec}"}
    `#{exec}`

    exec = "gcloud config set project null"
    Cli::LOG.debug(self) {"#{exec}"}
    `#{exec}`

    puts "job is done!"
  end







end