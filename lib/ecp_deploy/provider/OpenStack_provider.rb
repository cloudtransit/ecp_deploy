#!/usr/bin/env ruby
require_relative "provider"
require 'yaml'
require 'net/http'
require 'uri'
require 'json'


# Openstack specific provider class that implements missing functions from provider class
# that are needed to deploy a kubernetes cluster using the openstack command line interface.

# Author:: Arne Salveter, Christian-Friedrich St�ben
class OpenStack < Provider

  attr_accessor :credentials, :provideruid, :master_ip

  # Constructor needs openstack credentials
  def initialize(credentials)
    @credentials = credentials
    @provideruid = 'OpenStack'
    verify_login_and_network
  end

  def getStartingTime(machine)


   return Time.now.to_i
  end


  # Verifies network-id and converts network to uuid
  def verify_login_and_network
    begin
      if @credentials.network_name.nil? || @credentials.network_name==''
        Cli::LOG.info('Testing login credentials')
        exec = "openstack --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-username #{@credentials.userID} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} image list"
        Cli::LOG.debug(self) { "Running command: #{exec}" }
        `#{exec}`
      else
        Cli::LOG.info('Getting network id')
        exec = "openstack --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-username #{@credentials.userID} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} network show #{@credentials.network_name}"
        Cli::LOG.debug(self) { "Running command: #{exec}" }
        ret = `#{exec}`

        @network_name = ret.firstlineoccured(' id ').split('|')[2].gsub(' ', '') unless ret.nil? || ret==''
        

      end
    rescue => exception
      Cli::LOG.fatal(self) { "Openstack cli not installed. E: #{exception.inspect}" }
    ensure
      unless $?.exitstatus==0
        Cli::LOG.fatal(self) { 'Login credentials not working.' }
        exit(false)
      end
    end
  end


  # Adds security rules to a given cluster using openstack cli
  def generatesecurityrules(cluster)

    ports = cluster.open_ports + NEEDED_PORTS

    ports.each do |port|
      exec = "openstack --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-username #{@credentials.userID} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} security group rule create --src-ip #{@credentials.network_CIDR} --dst-port #{port} --proto tcp #{cluster.secgrp_name}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`
      Cli::LOG.info("Added port: #{port}")
      unless $?.exitstatus==0
        Cli::LOG.warn(self) { "Port #{port} could not be set." }
      end
    end

  end


  # Add rule
  #
  # @param cluster A running cluster
  # @param port defines the network port
  # @param protocol defines the network protocol
  # @param cidr is the classless inter-domain routing defnition
  def addsecurityrule(port,protocol,cluster)
    Cli::LOG.info(self){"open port #{port}"}
    exec = "openstack --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-username #{@credentials.userID} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} security group rule create --src-ip #{@credentials.network_CIDR} --dst-port #{port} --proto #{protocol} #{cluster.secgrp_name}"
    Cli::LOG.debug(self){"Running command: #{exec}"}
    `#{exec}`
  end


  # Add a security rule for the storage cluster
  #
  # @param cluster A running cluster
  def addstoragesecurityrules(cluster)
    addsecurityrule("111","tcp",cluster)
    addsecurityrule("111","udp",cluster)
    addsecurityrule("2049","udp",cluster)
    addsecurityrule("24007","tcp",cluster)
    addsecurityrule("24008","tcp",cluster)
    addsecurityrule("38465","tcp",cluster)
    addsecurityrule("38466","tcp",cluster)
    addsecurityrule("38467","tcp",cluster)
    addsecurityrule("49152","tcp",cluster)
    addsecurityrule("49153","tcp",cluster)
    from_port = "2003"
    to_port = "7300"
    protocol = "tcp"
    Cli::LOG.info(self){"Add storage security rule"}
    exec = "openstack --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-username #{@credentials.userID} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} security group rule create --src-ip #{@credentials.network_CIDR} --dst-port #{from_port}:#{to_port} --proto #{protocol} #{cluster.secgrp_name}"
    Cli::LOG.debug(self){"Running command: #{exec}"}
    `#{exec}`
  end


  # Generates a security group for a cluster in openstack
  def generatesecuritygrp(cluster)
    #secname = 'sg_'+@name.gsub(' ', '')
    secname = cluster.get_next_uid
    exec="openstack security group create --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain}  --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} #{secname}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
    cluster.secgrp_name = secname
    unless $?.exitstatus==0
      Cli::LOG.fatal(self) { 'Security group could not be created.' }
      exit(false)
    end
  end

  # Deletes the securitygroup of a cluster from openstack
  def deletesecuritygrp(cluster)
    exec="openstack security group delete --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} #{cluster.secgrp_name}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
    status = $?.exitstatus==0
    unless status
      Cli::LOG.warn(self) { "Security group #{cluster.secgrp_name} could not be deleted." }
    end
    status
  end


  # Create the storage machine
  #
  # @param machine A object, that describe a storage node
  # @param sec_group The security-group for the machine
  def generateStorage(machine,secgrp_name)
    generate_server(machine,secgrp_name,@storageUserDataFile)
    @master_uid = machine.uid
    Cli::LOG.info("Generated storage with uid: #{machine.uid}")
    @keypair = @credentials.key_name
    @network_CIDR = @credentials.network_CIDR
    @network_name = @credentials.network_name
    @volume_name = @credentials.volume_name
  end

  # Getting existing ip address of machine
  def get_machine_ip(machine)
    machine_ip = nil
    while machine_ip.nil? do
      exec = "openstack --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} server show #{machine.uid}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      tmp_ip = `#{exec}`
      machine_ip = tmp_ip.firstlineoccured('addresses').split('|')[2].split('=')[1]
    end
    Cli::LOG.debug(self) { "IP of machine #{machine.uid} is #{machine_ip}" }
    machine_ip.gsub(' ', '')
  end


  def generate_server(machine, secgrp_name, datafile)
    #tmpname = @name.to_s.gsub(/\s+/, '') + machine.role
    exec = "openstack server create --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url}  --image #{machine.image} --flavor #{machine.flavor}  --security-group #{secgrp_name} --user-data #{datafile}"
    exec += " --nic net-id=#{@credentials.network_name}" unless @credentials.network_name.nil?
    exec += " --key-name #{@credentials.key_name}" unless @credentials.key_name.nil?
    exec += " #{machine.hostname}"

    Cli::LOG.debug(self) { "Running command: #{exec}" }
    
      tmp_out = `#{exec}`
    begin
      machine.uid = tmp_out.firstlineoccured(' id ').split('|')[2].gsub(' ', '')
    rescue NoMethodError
      machine.uid = nil
    ensure
      if machine.uid == nil
        Cli::LOG.fatal(self) { 'Could not create machine. Aborting' }
        exit(false)
      end
    end
  end

  # Requests a floating ip using the openstcack cli
  def generateFloatingIP
#    tmp_out = `openstack ip floating create --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} #{@credentials.ip_pool}`
#    @master_ip = tmp_out.firstlineoccured(" ip").split("|")[2].gsub(" ", "").gsub("\n", "")
#    puts "Master is running on: #{@master_ip}"
  end

  # Associates a floating ip to the master server
  def assocFloatingIP
#    tmp_out="openstack ip floating add --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} #{@master_ip} #{@master_uid}"
#puts tmp_out
#    exec = `#{tmp_out}`
#    puts "Floating ip associated for #{@master_uid} to #{@master_ip}"
    while @master_ip.nil? do
      exec = "openstack --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} server show #{@master_uid}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      tmp_ip = `#{exec}`
      @master_ip = tmp_ip.firstlineoccured('addresses').split('|')[2].split('=')[1]
    end
    @master_ip = @master_ip.gsub(' ', '')
    Cli::LOG.info("Master IP is: #{@master_ip}")
  end

  # Generates cloud-init script for master server using a given template and the generated main certificate.
  def generateMasterUserdata(etcd_token, hostname)
    generate_master_userdata(etcd_token,hostname ,'$private_ipv4')
  end

  # Generates master-update-userdata
  def masterupdateuserdata(etcd_name, etcd_ic_list)
    super(etcd_name,etcd_ic_list,'$private_ipv4')
  end

  # Deletes a server from openstack
  def deletemachine(machine)
    exec = "openstack server delete #{machine.uid} --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} "
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
    unless $?.exitstatus==0
      Cli::LOG.warn(self) { "Could not delete machine #{machine.uid}." }
    end
  end

  # Releases a floating ip
  def remove_floating_ip(cluster)
    #remove floating ip
    #    tmp_out = `openstack ip floating list --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url}`
    #    ip_id = tmp_out.firstlineoccured(cluster.public_ipv4.to_s).split("|")[1].gsub(' ','').gsub('\n','')
    #    tmp_out = `openstack ip floating delete #{ip_id} --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-project-name #{@credentials.tenant} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url}`
  end

  # SSH send command
  #
  # @param host is the machine hostname
  # @param cmd is the command to execute
  def sshSend(host,cmd)
    tmp_exe = "ssh #{host} '#{cmd}'"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
  end

  # deprecated, use get_machine_ip instead
  def getIP(hostname,uid)
    tmp_exe="openstack server list --os-username #{@credentials.userID} --os-password #{@credentials.password} --os-auth-url #{@credentials.auth_url} --os-project-name #{@credentials.tenant}  --os-domain-name #{@credentials.project_domain} --os-project-domain-name #{@credentials.project_domain} --name #{hostname} -c Networks -f value"
    Cli::LOG.debug(self){"Running command: #{tmp_exe}"}
    tmp_out = `#{tmp_exe}`
    return tmp_out.sub!("Ext-Net=","").strip
  end


  # Check machine system state
  #
  # @param cluster A running cluster
  # @param uid is the machine uid
  def checkMachineSystemState(cluster,uid,role)
    if role.downcase =="storage" || role.downcase =="mon" || role.downcase =="osd"
      seconds = 45
      Cli::LOG.info("Waiting for #{seconds} seconds for the machine with uid #{uid} to start...")
      sleep(seconds)
    elsif role.downcase =="master" || role.downcase =="lowperformer"
      seconds = 40
      Cli::LOG.info("Waiting for #{seconds} seconds for the machine with uid #{uid} to start...")
      sleep(seconds)
    end
    
  end
  
  def attachStorageVolumes(cluster)
    Cli::LOG.info("Nothing to do...")
  end

end
