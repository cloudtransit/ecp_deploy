#!/usr/bin/env ruby

require_relative "../storage/storage.rb"
require_relative "../storage/cephfs_storage.rb"
require_relative "../storage/glusterfs_storage.rb"
require_relative "../storage/flocker_storage.rb"

# Basic provider class that implements steps needed to deploy
# a kubernetes cluster.

# Author:: Arne Salveter, Christian-Friedrich St�ben
class Provider

  MASTER_TEMPLATE='../templates/master_template.yaml'
  MASTER_UPDATE_TEMPLATE='../templates/master_update_template.yaml'
  SLAVE_TEMPLATE='../templates/slave_template.yaml'
  SEGURITY_GROUP="../templates/securityGroup.json"


  NEEDED_PORTS=[2379, 2380, 4001, 4194, 6443, 7001, 8080, 10248, 10250, 15441]

  attr_accessor :name, :version
  attr_accessor :cacertfile, :cakeyfile
  attr_accessor :credentials, :provideruid, :master_ip

  def initialize

  end


  # Create initial cluster
  #
  # * Generates directories for certificates
  # * Generates certificates
  # * Generates security group
  # * Adds rules to security group
  # * Generates floating IP
  # * Generates master and slave cloud-init scripts
  # * Generates master and slave servers
  # * Associates floating ip to master server
  # * Generates admin credentials for kubectl
  # * Configures kubectl to use generated credentials
  # * Deletes temporary generated cloud-init scripts
  # * Saves cluster status
  #
  # @param cluster Instance of cluster
  # @param status_file File location the cluster is saved
  def createCluster(cluster, status_file)
    @name=cluster.name

    Cli::LOG.info('Generating directories...')
    generate_directories(cluster, status_file)

    Cli::LOG.info('Generating cluster certificates...')
    generate_ca_cert(cluster)

    Cli::LOG.info('Creating security group and adding rules...')
    generatesecuritygrp(cluster)

    Cli::LOG.info("Created security group: #{cluster.secgrp_name}")
    generatesecurityrules(cluster)
    
    generateFloatingIP
    
    unless cluster.storage_cluster.nil?   
      addstoragesecurityrules(cluster)
      storagecluster = create_storage(cluster.storage_cluster)
    end
    
    cluster.machines.each do |machine|
      clusterTyp = ['mon', 'osd', 'storage', 'flocker']
      role = machine.role.downcase

        if clusterTyp.include? role
          generateStorageUserdata(cluster)
          generateStorage(machine, cluster.secgrp_name)
        end
      end

    cluster.keypair = @keypair
    cluster.storage_cluster_network = @network_CIDR
    cluster.volume_name = @volume_name

    unless storagecluster.nil?   
      checkStorageClusterMachineState(cluster)
      getStorageClusterMachineIP(cluster)
      storagecluster.prepare(cluster)
      attachStorageVolumes(cluster)
      storagecluster.generate(cluster)
    end

    Cli::LOG.info('Generating startup scripts...')

    #generateSlaveUserdata
    etcd_token = get_etcd_token(cluster.master_count)

    cluster.machines.each do |machine|
      if machine.role.downcase =='master'
        generateMasterUserdata(etcd_token, machine.hostname)
        generateMaster(machine, cluster.secgrp_name)
        assocFloatingIP
        cluster.public_ipv4 = @master_ip

      end
    end

    cluster.machines.each do |machine|
      unless machine.role.downcase =='master' || machine.role =="mon" || machine.role == "osd" || machine.role == "storage"
        generateSlaveUserdata(machine.hostname)
        generateSlave(machine, cluster.secgrp_name)
        machine.starting_time = Time.now.to_i
      end
    end
    
    checkMasterSlaveMachineState(cluster) unless storagecluster.nil?    
    getMasterSlaveIP(cluster)
    storagecluster.finalize(cluster) unless storagecluster.nil?

    Cli::LOG.info('Cleaning up and saving cluster')
    #deleteuserdata
    savecluster(cluster, status_file)

    Cli::LOG.info('Generating login certificates.')
    generateadmincerts(cluster)

    Cli::LOG.info('Setting up kubectl.')
    configurekubectl(cluster)
  end

  # Generates directories needed to store cluster status and certificates
  #
  # @param cluster Instance of cluster
  # @param status_file File location the cluster is saved
  def generate_directories(cluster, status_file)
    base_path = File.dirname File.absolute_path status_file
    cluster.ssl_path = (base_path+'/'+@name).gsub(/\s+/, '')
    exec = "mkdir -p #{cluster.ssl_path}"
    Cli::LOG.debug(self) { "Running command: #{exec}" }
    `#{exec}`
  end

  # Generates admin certificate and private key for cluster access
  #
  # @param cluster Instance of cluster
  def generateadmincerts(cluster)

    # Setting file paths for admin private key and admin certificate
    cluster.adm_pem = "adm-#{cluster.name}.pem".gsub(/\s+/, '')
    cluster.adm_key = "adm-#{cluster.name}-key.pem".gsub(/\s+/, '')

    @adm_key =cluster.ssl_path+'/'+cluster.adm_key
    adm_csr = cluster.ssl_path+'/adm.csr'
    @adm_pem = cluster.ssl_path+'/'+cluster.adm_pem

    begin
      # Generate admin key
      exec = "openssl genrsa -out #{@adm_key} 2048"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Request verified certificate
      exec = "openssl req -new -key #{@adm_key} -out #{adm_csr} -subj \"/CN=kube-admin\""
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Verfify admin certificate
      exec = "openssl x509 -req -in #{adm_csr} -CA #{@cacertfile} -CAkey #{@cakeyfile} -CAcreateserial -out #{@adm_pem} -days 365"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Delete request file
      exec = "rm #{adm_csr}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

    rescue
      Cli::LOG.fatal(self) { 'Openssl not installed.' }
    ensure
      unless $?.exitstatus==0
        Cli::LOG.fatal(self) { 'Openssl not working correctly. Aborting' }
        exit(false)
      end
    end

  end

  # Configures kubectl to use the generated admin credentials for cluster access
  #
  # @param cluster Instance of cluster
  def configurekubectl(cluster)
    begin
      # Setting local file paths
      clustername = cluster.name.gsub(/\s+/, '')
      abs_ca_pem = cluster.ssl_path+'/'+cluster.ca_pem
      abs_adm_key = cluster.ssl_path+'/'+cluster.adm_key
      abs_adm_pem = cluster.ssl_path+'/'+cluster.adm_pem

      # Adds cluster
      exec = "kubectl config set-cluster #{clustername} --server=https://#{cluster.public_ipv4}:6443 --certificate-authority=#{abs_ca_pem}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Adds admin-user
      exec = "kubectl config set-credentials #{clustername}-admin --certificate-authority=#{abs_ca_pem} --client-key=#{abs_adm_key} --client-certificate=#{abs_adm_pem}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Setting context
      exec = "kubectl config set-context #{clustername}-system --cluster=#{clustername} --user=#{clustername}-admin"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      exec = "kubectl config use-context #{clustername}-system"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`
    rescue
      Cli::LOG.error(self) { 'Error while configuring kubectl.' }
    ensure
      unless $?==0
        Cli::LOG.error(self) { 'Kubectl not working, skipping' }
      end
    end
  end

  # Unsets kubectl config used for this cluster
  #
  # @param cluster Instance of cluster
  def unsetkubectl(cluster)
    begin
      clustername = cluster.name.gsub(/\s+/, '')

      exec = "kubectl config unset clusters.#{clustername}"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      exec = "kubectl config unset contexts.#{clustername}-system"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      exec = "kubectl config unset users.#{clustername}-admin"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`
    rescue
      Cli::LOG.error(self) { 'Error while unsetting kubectl config.' }
    ensure
      unless $?==0
        Cli::LOG.error(self) { 'Kubectl not working, skipping' }
      end
    end
  end

  # Sets certificate files for deployed servers
  #
  # @param cluster Instance of cluster
  def set_ca_files(cluster)

    # Set names for private-key and certificate files
    cluster.ca_pem = "ca-#{cluster.name}.pem".gsub(/\s+/, '')
    cluster.ca_key = "ca-#{cluster.name}-key.pem".gsub(/\s+/, '')

    @cacertfile = cluster.ssl_path+'/'+cluster.ca_pem
    @cakeyfile = cluster.ssl_path+'/'+cluster.ca_key
    @ssl_path = cluster.ssl_path
  end

  # Generates new private key and main certificate to verify cluster certificates
  #
  # @param cluster Instance of cluster
  def generate_ca_cert(cluster)
    @name=cluster.name

    # Read certificate and private key path from cluster
    set_ca_files(cluster)

    begin
      # Generate new private key
      exec = "openssl genrsa -out #{@cakeyfile} 2048"
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`

      # Generate certificate using private key
      exec = "openssl req -x509 -new -nodes -key #{@cakeyfile} -days 10000 -out #{@cacertfile} -subj \"/CN=kube-ca\" "
      Cli::LOG.debug(self) { "Running command: #{exec}" }
      `#{exec}`
    rescue
      Cli::LOG.fatal(self) { 'Openssl could not be started.' }
    ensure
      unless $?.exitstatus==0
        Cli::LOG.fatal(self) { 'Openssl configure incomplete. Aborting.' }
        exit(false)
      end
    end
  end

  # Saves cluster status to yaml file
  #
  # @param cluster Instance of cluster
  # @param cluster_file The status file the cluster is saved into
  def savecluster(cluster, cluster_file)
    @name=cluster.name
    File.open(cluster_file, 'w') { |file|
      file.write(YAML::dump(cluster))
    }
  end

  # Get master template from gem
  def master_template
    File.join(File.dirname(File.expand_path(__FILE__)), MASTER_TEMPLATE)
  end

  # Get slave template from gem
  def slave_template
    File.join(File.dirname(File.expand_path(__FILE__)), SLAVE_TEMPLATE)
  end

  def storage_template(cluster)
    File.join(File.dirname(File.expand_path(__FILE__)),"../templates/#{cluster.storage_cluster}_storage_template.yaml")
  end

  # Get master update template from gem
  def master_update_template
    File.join(File.dirname(File.expand_path(__FILE__)), MASTER_UPDATE_TEMPLATE)
  end


  def security_group
    File.join(File.dirname(File.expand_path(__FILE__)),SEGURITY_GROUP)
  end

  def get_etcd_token(master_count)
    # Get etcd discovery token
    etcd_token = Net::HTTP.get_response(URI.parse("https://discovery.etcd.io/new?size=#{master_count}")).body
    Cli::LOG.debug(self) { "Discovery token: #{etcd_token}" }
    etcd_token
  end

  # Generates cloud-init script for master server using a given template and the generated main certificate.
  def generate_master_userdata(etcd_token, master_hostname, cluster_ip)
    @masterUserData = File.read master_template #File.read(MASTER_TEMPLATE)
    
    # Certificate
    ca_data = File.read(@cacertfile)

    # Private key for certificate
    key_data = File.read(@cakeyfile)

    # Add certificate and private key to cloud-init script
    tmpdata = @masterUserData.gsub("<ca-cert.pem>\n", ca_data.whitespacetoalllines(6)).gsub("<ca-key.pem>\n", key_data.whitespacetoalllines(6)).gsub("<master-ip>", cluster_ip).gsub("<discovery-token>", etcd_token).gsub("<storage-ip>",@storage_ip.to_s).gsub('<master-hostname>', master_hostname)

    # Save master-template temporary
    @masterUserDataFile=@ssl_path+"/master#{@name.to_s}".gsub(/\s+/, '')
    File.open(@masterUserDataFile, 'w') { |file| file.write(tmpdata) }
    Cli::LOG.debug(self) { "Generated master-userdata: #{@masterUserDataFile}" }
  end

  # Generates cloud-init script for compute node server using a given template and the generated main certificate.
  def generateSlaveUserdata(slave_hostname)
#    @slaveUserData = File.read(SLAVE_TEMPLATE).gsub("<master-ip>",@master_ip.to_s).gsub("<slave-ip>","$private_ipv4")

    @slaveUserData = File.read(slave_template).gsub('<master-ip>', @master_ip.to_s).gsub('<slave-ip>', '$private_ipv4').gsub('<slave-hostname>',slave_hostname)

    # Certificate
    ca_data = File.read(@cacertfile)

    # Private key for certificate
    key_data = File.read(@cakeyfile)

    # Add certificate and private key to cloud-init script
    @slaveUserData = @slaveUserData.gsub("<ca-cert.pem>\n", ca_data.whitespacetoalllines(6)).gsub("<ca-key.pem>\n", key_data.whitespacetoalllines(6)).gsub("<storage-ip>",@storage_ip.to_s)

    # Save slave-template temporary
    @slaveUserDataFile=@ssl_path+"/slave#{@name.to_s}".gsub(/\s+/, '')
    File.open(@slaveUserDataFile, 'w') { |file| file.write(@slaveUserData) }
  end

  # Generates init script for storage server using a given template
  def generateStorageUserdata(cluster)
    @storageUserData = File.read(storage_template(cluster))
    @storageUserDataFile=@ssl_path+"/storage#{@name.to_s}".gsub(/\s+/,"")
    File.open(@storageUserDataFile, 'w') { |file| file.write(@storageUserData) }
  end

  # Delete specific cloud-init scripts generated from templates
  def deleteuserdata
    File.delete(@slaveUserDataFile) if @slaveUserDataFile!=nil and File.exists? @slaveUserDataFile
    File.delete(@masterUserDataFile) if @masterUserDataFile!=nil and File.exists? @masterUserDataFile
    File.delete(@storageUserDataFile) if @storageUserDataFile!=nil and File.exists? @storageUserDataFile
    File.delete(@storageAdminUserDataFile) if @storageAdminUserDataFile!=nil and File.exists? @storageAdminUserDataFile
  end

  # Deletes the cluster.
  #
  # * Deletes all machines
  # * Unsets kubectl config
  # * Deletes securitygroup
  # * Deletes floating ip
  # * Deletes files and directories created by cluster
  #
  # @param cluster The cluster that will be deleted
  # @param status_file The file the cluster was stored in
  def deletecluster(cluster, status_file)

    Cli::LOG.info('Deleting machines...')
    cluster.machines.each do |machine|
      deletemachine(machine)
    end


    cluster.rmAllMachine


    Cli::LOG.info('Unsetting kubectl config...')
    unsetkubectl(cluster)

    Cli::LOG.info('Removing security group...')

#		 FileUtils.rm_rf(cluster.ssl_path)

# workaround, to to replace FileUtils
    delete_dir(cluster.ssl_path)
#		if File.directory?(cluster.ssl_path)
#			Dir.removemdir(cluster.ssl_path)
#		end

    remove_floating_ip(cluster)
    status = deletesecuritygrp(cluster)

    unless status
      question = "One more try, to delete the security-group?\n (Y)es | (N)o"
      input = Cli::user_input_helper(question, 8)
      unless input.nil? or input.downcase=='n'
        deletesecuritygrp(cluster)
      end
    end
    Cli::LOG.info('Deleting files...')
    File.delete(status_file)
  end

  # Generate userdatafile for master update
  def masterupdateuserdata(etcd_name, etcd_ic_list,ssl_ip)
    # Generate initial cluster string
    etcd_ic_str = ''
    etcd_ic_list.each do
    |member|
      etcd_ic_str += "#{member['id']}=#{member['peerURLs'][0]},"
    end
    etcd_ic_str+="#{etcd_name}=http://$private_ipv4:2380"
    Cli::LOG.debug(self) { "Initial cluster: #{etcd_ic_str}" }

    userdata = File.read master_update_template

    # Certificate
    ca_data = File.read(@cacertfile)

    # Private key for certificate
    key_data = File.read(@cakeyfile)


    # Create local userdata file
    userdata = userdata.gsub('<etcd-name>', etcd_name)
    userdata = userdata.gsub('<ic-members>', etcd_ic_str)
    userdata = userdata.gsub("<ca-cert.pem>\n", ca_data.whitespacetoalllines(6))
    userdata = userdata.gsub("<ca-key.pem>\n", key_data.whitespacetoalllines(6))
    userdata = userdata.gsub('<master-ip>', @master_ip.to_s)
    userdata = userdata.gsub('<openssl-ip>', ssl_ip)

    # Save userdata file
    updatefile=@ssl_path+"/master_upd_#{@name.to_s}".gsub(/\s+/, '')
    File.open(updatefile, 'w') { |file| file.write(userdata) }
    updatefile
  end

  # Generates a master server using a generated cloud-init script
  def generateMaster(machine, secgrp_name)
    generate_server(machine, secgrp_name, @masterUserDataFile)
    @master_uid = machine.uid
    machine.starting_time = getStartingTime(machine)
    Cli::LOG.info("Generated master with uid: #{machine.uid}")
  end

  # Generates a compute node server using a generated cloud-init script
  def generateSlave(machine, secgrp_name)
    generate_server(machine, secgrp_name, @slaveUserDataFile)
    machine.starting_time = getStartingTime(machine)
    Cli::LOG.info("Generated slave with uid: #{machine.uid}")
  end


  # Inserts new master into existing etcd2 cluster
  def updatemaster(machine, secgrp_name)

    # Setup connection to etcd2 service
    uri = URI.parse("http://#{@master_ip}:2379")
    http = Net::HTTP.new(uri.host, uri.port)

    # Get etcd2 cluster members
    etcd_members = JSON.parse(http.send_request('GET', '/v2/members').body)['members'] #Net::HTTP.get_response(URI.parse("http://#{@master_ip}:2380/members")).body

    # Adding new dummy to cluster
    header = {'Content-Type': 'application/json'}
    response = http.send_request('POST', '/v2/members', '{"peerURLs":["http://127.0.0.1:2380"]}', header).body
    #puts response
    etcd_id=JSON.parse(response)['id']

    # Generate master-update userdata
    userdata = masterupdateuserdata(etcd_id, etcd_members)

    # Generate new master
    generate_server(machine, secgrp_name, userdata)
    Cli::LOG.info("Generated master with uid: #{machine.uid}")

    # Getting new master ip
    master_ip = get_machine_ip machine

    # Replace dummy ip with existing master ip
    Cli::LOG.debug(self) { "Replacing ip of #{etcd_id} in etcd2 cluster with #{master_ip}" }

    body = '{"peerURLs":["http://'+"#{master_ip}"+':2380"]}'
    path = "/v2/members/#{etcd_id}"
    Cli::LOG.debug(self) { "path: #{path} body: #{body} header: #{header}" }
    response = http.send_request('PUT', path, body, header)
    Cli::LOG.error(self) { "Error: #{response.body}" } unless (response.body == '' || response.body == nil)
    waitforetcd2member http, etcd_id
  end

  # Make deployer wait for connecting etcd2 member
  def waitforetcd2member(http, etcd_id)
    loaded = false
    until loaded
      Cli::LOG.info('waiting for etcd2 member to connect...')
      etcd_members = JSON.parse(http.send_request('GET', '/v2/members').body)['members']
      etcd_members.each do |member|
        loaded = true if (member['id']==etcd_id && member['name'] != '')
      end
      sleep 10
      # TODO: Konstante anpassen, ggf timeout und abbruch?
    end
    Cli::LOG.info("New etcd2 member #{etcd_id} has connected")

  end

  # Recursive directory delete, like rm -rf <dir>
  def delete_dir(dir)
    if File.directory?(dir)
      Dir.foreach(dir) do |file|
        delete_dir("#{dir}/#{file}") unless (file.to_s == '.' or file.to_s == '..')
      end
      Dir.delete(dir)
    else
      File.delete(dir)
    end
  end

  # Check if the cluster is realy up/down.
  #
  # * check if all machines are up/down
  # * show an waiting-bar
  # * is possible overwritten in a special provider
  # @param cluster The cluster that will be deleted
  # @param status_file The file the cluster was stored in
  def readyCheck(cluster)

    Cli::LOG.info("A cluster is starting on #{@master_ip}. This may take some time. Use kubectl to control your cluster.")

  end

  # Create the firewall rules.
  def addsecurityrules(cluster)
      Cli::LOG.info(self){"Start: adding security rules"}
      @securityGroupFile = File.read(security_group)
      @securityGroupHash = JSON.parse(@securityGroupFile)

      begin
         rulehash = @securityGroupHash["rule"]
         raise IOError.new('Missing rule description') if rulehash.nil?
         rulehash.each do |hash|
           addsecurityrule(cluster,hash["port"],hash["protocol"],cluster.storage_cluster_network)
         end

         cluster.open_ports.each do |port|
           addsecurityrule(cluster,port,"tcp","0.0.0.0/0")
         end
         addstoragesecurityrule(cluster)

         rescue IOError=>e
            Cli::LOG.fatal(self){"Error parsing security group file: #{e.inspect}"}
            exit(false)
         end

         Cli::LOG.info(self){"done: Adding security rules"}
  end



  def create_storage(storage_cluster)
     storage = nil

     if storage_cluster.downcase == "cephfs"
       storage = CephfsStorage.new
     end
     if storage_cluster.downcase == "glusterfs"
       storage = GlusterfsStorage.new
     end
     if storage_cluster.downcase == "flocker"
       storage = FlockerStorage.new
     end
     Cli::LOG.debug(self){"empty"}
     storage
   end

   def checkStorageClusterMachineState(cluster)
     cluster.machines.each do |machine|
       if Storage.getClusterRoles.include? machine.role
         checkMachineSystemState(cluster,machine.uid,machine.role)
       end
     end
   end


  def checkMasterSlaveMachineState(cluster)
    cluster.machines.each do |machine|
      if !(Storage.getClusterRoles.include? machine.role)
        checkMachineSystemState(cluster,machine.uid,machine.role)
      end
    end
  end

  def getStorageClusterMachineIP(cluster)
    cluster.machines.each do |machine|
      if Storage.getClusterRoles.include? machine.role
        machine.private_ipv4 = getIP(machine.hostname,machine.uid)
        @storage_ip = machine.private_ipv4
      end
    end
  end

  def getMasterSlaveIP(cluster)
    cluster.machines.each do |machine|
      if !(Storage.getClusterRoles.include? machine.role)
        machine.private_ipv4 = get_machine_ip(machine)
      end
    end
  end


  # A last order for a nice success message.
  #
  # could be overwritten in a special provider
  def lastOrder()
    Cli::LOG.info('job is done!')
  end

end
