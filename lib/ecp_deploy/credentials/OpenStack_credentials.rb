#!/usr/bin/env ruby

require_relative "credentials"

# A special class that holds the OpenStack credential logic

# Author:: Arne Salveter, Christian-Friedrich Stüben
class OSCredentials < Credentials
	attr_accessor :tenant, :auth_url, :ip_pool, :project_domain, :user_domain, :network_CIDR, :network_name, :volume_name, :key_name
	def initialize(providerName, userID, password, tenant, auth_url, ip_pool)
		super(providerName, userID, password)
		@tenant = tenant
		@auth_url = auth_url
		@ip_pool = ip_pool
		Cli::LOG.debug(self){"OS-Credentials: #{providerName}, #{userID}, #{password}, #{tenant}, #{auth_url}, #{ip_pool}"}
	end
end


