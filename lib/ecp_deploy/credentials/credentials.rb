#!/usr/bin/env ruby


# Basic class that holds the basic credentials 

# Author:: Arne Salveter, Christian-Friedrich Stüben
class Credentials

  attr_accessor :providerName, :userID, :password, :tenant, :auth_url

	def initialize(providerName, userID, password)
		@providerName = providerName
		@userID = userID
		@password = password
		@tenant = tenant
		@auth_url = auth_url
	end

	def to_s
		providerName
	end
end

