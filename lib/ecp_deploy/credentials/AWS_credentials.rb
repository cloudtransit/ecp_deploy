#!/usr/bin/env ruby

require_relative "credentials"

# A special class that holds the aws credential logic 

# Author:: Arne Salveter, Christian-Friedrich Stüben
class AWSCredentials < Credentials
	attr_accessor  :region, :output, :key_pair, :network_CIDR, :volume_name
	def initialize(name, id, password, region, output, key_pair, network_CIDR, volume_name)
		super(name, id, password)
		@output = output
		@region = region
		@key_pair = key_pair
    @network_CIDR = network_CIDR
    @volume_name = volume_name
		setTmpData
		Cli::LOG.debug(self){"AWS-Credentials: #{name}, #{id}, #{password}, #{region}, #{output}, #{key_pair}"}
	end

	def setTmpData()
		puts "Set up temp AWS-Credentials"
		ENV['AWS_ACCESS_KEY_ID'] = "#{@userID}"
		ENV['AWS_SECRET_ACCESS_KEY'] = "#{@password}"
	end


end


