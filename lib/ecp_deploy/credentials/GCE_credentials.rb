#!/usr/bin/env ruby

require_relative "credentials"

# A special class that holds the Google Compute Engine credential logic

# Author:: Arne Salveter, Christian-Friedrich Stüben
class GCECredentials < Credentials
  attr_accessor :region, :key_pair, :image_project, :project_id, :key_file_path, :service_account

  def initialize(name, project_id, service_account, key_file_path, region, key_pair, image_project)
    super(name, "", "")
    @region = region
    @key_pair = key_pair

		@image_project = image_project	# TODO: move to simpleCluster-file

		@project_id = project_id
		@service_account = service_account		
		@key_file_path = key_file_path


		setLoginData
		Cli::LOG.debug(self) {"GCE-Credentials: #{name}, #{userID}, #{region}"}
  end

	def setLoginData()
    Cli::LOG.debug(self) {"Set up temp GCE-Credentials"}
		exec = "gcloud auth activate-service-account #{@service_account} --key-file #{@key_file_path}"
		Cli::LOG.debug(self) {"#{exec}"}
		`#{exec}`
		
		exec = "gcloud config set account #{@service_account}"
		Cli::LOG.debug(self) {"#{exec}"}
		`#{exec}`
	
		exec = "gcloud config set project #{@project_id}"
		Cli::LOG.debug(self) {"#{exec}"}
		`#{exec}`
  end


end


