#!/usr/bin/env ruby

gem "json", ">= 1.8.3"
require 'json'

require_relative "../cluster/cluster"
require_relative "../cluster/machine"

require_relative "../credentials/credentials.rb"
require_relative "../credentials/OpenStack_credentials.rb"
require_relative "../credentials/AWS_credentials.rb"
require_relative "../credentials/GCE_credentials.rb"


# This class reads given files and parse them to usable objects. 

# Author:: Arne Salveter, Christian-Friedrich Stüben, Thomas Finnern
class Parser

  attr_accessor :inputHash, :inputHashPath, :cluser, :machines, :cluster_was_yaml
  attr_accessor :statusFile, :statusHash
  attr_accessor :credentialsFile, :credentialsHash, :credentialObject


  # Create the parser object
  #
  # @param new_cluster_path Path to description file.
  # @param sec_group Path to credential file.
  def initialize(new_cluster_path, credentials_path)
    @credentials = Array.new
    unless new_cluster_path==''
      @inputHashPath = new_cluster_path
    end
    @credentialsFile = File.read(credentials_path)
    @credentialsHash = JSON.parse(@credentialsFile)
    prepare_multiple_credentials(@credentialsHash)
  end


  def get_cluster
    cluster_new = read_cluster_json

    begin
    cluster_new = YAML::load(File.open(@inputHashPath)) if @cluster_was_yaml

    rescue SyntaxError => e
      Cli::LOG.fatal(self) {'The given yaml is not well formed.'}
      Cli::LOG.fatal(self) {"Error reading cluster-description: #{e.message}"}
      exit(false)
    end

    Cli::LOG.debug(self) {"Is given object a valid instance of \"Cluster\": #{cluster_new.instance_of? Cluster}"}
    cluster_new
  end


  # Create a cluster from a description file.
  def read_cluster_json
    begin
      @inputHash = JSON.parse(File.read(@inputHashPath))
      @cluster_was_yaml = false

      cluster_hash = @inputHash["cluster"]
      master_hash = @inputHash["master"]
      node_hash = @inputHash["nodes"]
      storage_hash = @inputHash["storage"]

      raise IOError.new('Missing cluster description') if cluster_hash.nil?
      raise IOError.new('Missing master description') if master_hash.nil?
      raise IOError.new('Missing nodes description') if node_hash.nil?

      cluster = Cluster.new(cluster_hash["name"])

      unless storage_hash.nil?
        storage_hash.each do |hash|
          count = 1
          hash["number"].times do
            cluster.addMachine(Machine.new(hash["provider"], hash["storage_cluster"], hash["image"], hash["flavor"], hash["role"], "#{hash['role']}#{count}"))
            cluster.storage_cluster = hash["storage_cluster"]
            count += 1
          end
        end
      end

      master_hash.each do |hash|
        count = 1
        hash["nodes"].times do
          cluster.addMachine(Machine.new(hash["provider"], nil, hash["image"], hash["flavor"], hash["role"], "#{hash['role']}#{count}"))
          count +=1
        end
        cluster.master_count = hash["nodes"]
      end
      node_hash.each do |hash|
        count = 1
        hash["number"].times do
          cluster.addMachine(Machine.new(hash["provider"], nil, hash["image"], hash["flavor"], hash["role"], "#{hash['role']}#{count}"))
          count +=1
        end
      end
      cluster.open_ports=cluster_hash["openports"]
    rescue SyntaxError => e
      Cli::LOG.fatal(self) {"Error parsing cluster file: #{e.inspect}"}
      exit(false)
    rescue JSON::ParserError => e
      # Assume input file was yaml
      @cluster_was_yaml = true
    end
    cluster
  end


  # Prepare credentials parsed from file and create
  # credentials objects
  def prepare_multiple_credentials(credentials_hash)
    hash = credentials_hash['credentials']
    if hash.nil?
      prepareCredential(credentials_hash)
    else
      hash.each do |creds|
        prepareCredential(creds)
      end
    end

    #TODO: Use multiple credentials and remove this
    @credentialObject = @credentials[0]

  end


  # Find credentials for specific provider
  def credentials_by_provider_name(provider_name)
    @credentials.find {|cred| cred.name == provider_name}
  end

  # Map the credentials from a Hash to the 	internal
  # attributes.
  def prepareCredential(credentials_hash)
    begin
      name = credentials_hash["provider"]
      acchash = credentials_hash["account"]

      raise IOError.new('Cannot find provider') if name.nil?
      raise IOError.new('Cannot find account values') if acchash.nil?

      id = acchash["id"]
      password = acchash["password"]

      if name.downcase == "openstack"
        tenant = acchash["tenant"]
        auth_url = acchash["auth-url"]
        ip_pool = acchash["ip-pool"]
        project_domain = acchash["project-domain"]
        user_domain = acchash["user-domain"]
        network_name = acchash["network-name"]
        network_CIDR = acchash["network-CIDR"]
        volume_name = acchash["volume-name"]
        key_name = acchash["key-name"]

        if id.nil? || id==''
          raise IOError.new('No user id set. Aborting.')
        end

        if password.nil? || password==''
          raise IOError.new('No password set. Aborting.')
        end

        if tenant.nil? || tenant==''
          raise IOError.new('No project set. Aborting.')

        end

        if auth_url.nil? || auth_url==''
          raise IOError.new('No authentication url set. Aborting.')
        end

        if project_domain.nil? || project_domain==''
          Cli::LOG.warn(self) {"No project domain set. Using 'default'."}
          project_domain = 'default'
        end

        if user_domain.nil? || user_domain==''
          Cli::LOG.warn(self) {"No user domain set. Using 'default'."}
          user_domain = 'default'
        end

        if network_name.nil? || network_name==''
          Cli::LOG.warn(self) {'No network name set. Will cause error when multiple networks are available.'}
        end

        tmp_creds = OSCredentials.new(name, id, password, tenant, auth_url, ip_pool) #
        tmp_creds.project_domain = project_domain
        tmp_creds.user_domain = user_domain
        tmp_creds.network_name = network_name
        tmp_creds.network_CIDR = network_CIDR
        tmp_creds.key_name = key_name
        tmp_creds.volume_name = volume_name
        Cli::LOG.warn('No key-name set in credentials. You may not be able to use ssh.') if key_name.nil?
        @credentials.push(tmp_creds)

      end
      if name.downcase == 'aws'
        output = acchash["output"]
        Cli::LOG.warn(self) {"No output format set. Using default."} if (output.nil? || output=='')

        region = acchash["region"]
        Cli::LOG.warn(self) {"No region set. Using default."} if (region.nil? || region=='')

        key_pair = acchash["key_pair"]
        Cli::LOG.warn(self) {'No key-pair set in credentials. You may not be able to use ssh.'} if (key_pair.nil?||key_pair=='')

        volume_name = acchash["volume_name"]
        Cli::LOG.warn(self) {'No volume set. You may not be able to use storage cluster'} if (volume_name.nil? || volume_name=='')

        network_CIDR = acchash["network_CIDR"]
        Cli::LOG.warn(self) {'No network CIDR set. You may not be able to use storage cluster'} if (network_CIDR.nil? || network_CIDR=='')

        @credentials.push(AWSCredentials.new(name, id, password, region, output, key_pair, network_CIDR, volume_name))
      end
      if name.downcase == 'gce'
        region = acchash["region"]
        Cli::LOG.warn(self) {"No region set. Using default."} if (region.nil? || region=='')

        image_project = acchash["image_project"]
        Cli::LOG.warn(self) {"No image_project set. Using default."} if (image_project.nil? || image_project=='')

        key_pair = acchash["key_pair"]
        Cli::LOG.warn(self) {'No key-pair set in credentials. You may not be able to use ssh.'} if (key_pair.nil?||key_pair=='')

        project_id = id
        service_account = acchash["service_account"]
        key_file_path = acchash["key_file_path"]

        @credentials.push(GCECredentials.new(name, project_id, service_account, key_file_path, region, key_pair, image_project))
      end
    end
  rescue IOError => e
    Cli::LOG.fatal(self) {"Error reading credentials: #{e.inspect}"}
    exit(false)
  end

end
