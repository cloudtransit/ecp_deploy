#!/usr/bin/env ruby

require_relative '../credentials/credentials.rb'
require_relative '../parser/parser.rb'

gem 'json', '>= 1.8.3'
require 'json'

gem 'hashdiff', '>= 0.0.6'
require 'hashdiff'

# Basic analyer class that mainly compare two cluster objects.


# Author:: Arne Salveter, Christian-Friedrich Stüben
class Analyser

  attr_accessor :credentials, :notworking, :providers

  def initialize(credentials)
    @credentials = credentials
    @notworking = Array.new
    @providers = Hash.new
  end

  def checkstatus(cluster)

  end

  # Compare two clusters
  #
  # * Generates a differential cluster object
  # * check witch machine ist running or down
  #
  # @param cluster_old Cluster form the object file.
  # @param cluster_new Cluster from parsed json file.
  # @param is_yaml_cluster New cluster is from object
  def compare(cluster_old, cluster_new, is_yaml_cluster)
    Cli::LOG.info('Comparing running cluster with new configuration. This may take a while.')
    machine_roles_old = Hash.new
    machine_roles_new = Hash.new
    machine_ips_old = Array.new
    machine_ips_new = Array.new

    # create role map for parsed cluster
    cluster_new.machines.each do |machine|
      machine_roles_old[machine.role] = (machine_roles_old[machine.role]) ? machine_roles_old[machine.role]+1 : 1
    end

    # remove machines in error state
    #checkstatus(cluster_old).each do |machine|
    #  cluster_old.rmmachine(machine)
    #  Cli::LOG.info("Removing malfunctioning machine: #{machine}")
    #end

    # Comparing ruby objects?
    if is_yaml_cluster
      cluster_old.machines.each do |current_machine|
        machine_ips_old.push(current_machine.private_ipv4)
      end

      cluster_new.machines.each do |new_machine|
        unless new_machine.private_ipv4.nil? || new_machine.private_ipv4==''
          machine_ips_new.push(new_machine.private_ipv4)
        end
      end

      #update initialized flags
      machine_ips_old.each do |ip|
        mach_new = cluster_new.get_machine_by_ip(ip)
        unless mach_new.nil?
          cluster_old.set_initialized_by_ip(ip,mach_new.initialized)
        end
      end

      delta_ips = machine_ips_old - machine_ips_new

      Cli::LOG.debug("Old cluster ips: #{machine_ips_old}")
      Cli::LOG.debug("New cluster ips: #{machine_ips_new}")

      delta_ips.each do |ip|
        tmp_machine = cluster_old.machines.select {|m| m.private_ipv4 == ip}[0]
        Cli::LOG.info("Delete machine #{tmp_machine.uid} with ip: #{tmp_machine.private_ipv4}.")
        @providers[tmp_machine.provider].deletemachine(tmp_machine)
        cluster_old.rmmachine(tmp_machine)
        cluster_old.machines.delete_if {|m| m.private_ipv4 == tmp_machine.private_ipv4}
      end
    end

    # create role map for parsed cluster
    cluster_old.machines.each do |machine|
      machine_roles_new[machine.role] = (machine_roles_new[machine.role]) ? machine_roles_new[machine.role]+1 : 1
    end
    Cli::LOG.info("Current cluster: #{machine_roles_new.to_s}")
    Cli::LOG.info("Cluster after update: #{machine_roles_old.to_s}")

    # Calculate differences
    delta_roles = HashDiff.diff(machine_roles_old, machine_roles_new)

    # Remmove differences
    delta_roles.each do |entry|

      case entry[0]
        when '+'
          # Additional role found, remove all unused machines.
          Cli::LOG.info("Removing unsused role #{entry[1]}")
          missing_machines = cluster_old.machines.select{|m| m.role==entry[1]}

          unless missing_machines.nil?
            missing_machines.each do |machine|
              @providers[machine.provider].deletemachine(machine)
              cluster_old.rmmachine(machine)
            end
          end

        when '-'
          Cli::LOG.info("Starting new machines for new role #{entry[1]}")
          # Missing role found, start all machines of this role


          missing_machines = cluster_new.machines.select {|m| m.role == entry[1]}
          prov = @providers[missing_machines[0].provider]
          prov.master_ip = cluster_old.public_ipv4
          prov.name = cluster_old.name
          prov.set_ca_files(cluster_old)


          missing_machines.each do |machine|
            prov.generateSlaveUserdata(machine.hostname)
            cluster_old.addMachine(machine)
            prov.generateSlave(machine, cluster_old.secgrp_name)
            machine.starting_time = Time.now.to_i
            machine.private_ipv4 = prov.get_machine_ip(machine)
            machine.initialized=false
          end
          prov.deleteuserdata

        when '~'

          Cli::LOG.info("Changing number of machines for role #{entry[1]}")

          # Number of role-machines updated
          if entry[2] > entry[3]

            # Missing machines, starting new ones
            missing_machines = cluster_new.machines.select {|m| m.role == entry[1]}

            # Filter existing machines if possible
            cluster_old.machines.each do |m_old|
              missing_machines.keep_if{|m_miss| m_miss.uid!=m_old.uid}
            end

            prov = @providers[missing_machines[0].provider]
            prov.master_ip = cluster_old.public_ipv4
            prov.name = cluster_old.name
            prov.set_ca_files(cluster_old)

            count = 0
            missing_machines.each do |machine|
              # dont start more than needed
              unless count >= (entry[2] - entry[3])
                cluster_old.addMachine(machine)
                if machine.role=='master'
                  prov.updatemaster(machine, cluster_old.secgrp_name)
                else
                  prov.generateSlaveUserdata(machine.hostname)
                  prov.generateSlave(machine, cluster_old.secgrp_name)
                end
                machine.starting_time = Time.now.to_i
                machine.private_ipv4 = prov.get_machine_ip(machine)
                machine.initialized=false
                count+=1
              end
            end
            prov.deleteuserdata
          else
            # Too many machines for this role, remove some
            missing_machines = cluster_old.machines.select {|m| m.role==entry[1]}

            count = 0
            (entry[3]-entry[2]).times do
              @providers[missing_machines[count].provider].deletemachine(missing_machines[count])
              cluster_old.rmmachine(missing_machines[count])
              count +=1
            end
          end
        else
          raise RuntimeError.new 'Unexpected entry in hasdiff'
      end
    end
    cluster_old
  end


  # Register a provider at the local object structur
  #
  # @param provider Instance of a special provider.
  def registerprovider(provider)
    @providers[provider.provideruid] = provider unless @providers.has_key?(provider.provideruid)
    Cli::LOG.debug(self) {"Register provider: #{provider}"}
  end

end

