require_relative "analyser"
require 'yaml'

# Analyser for GCE
class GCEanalyser < Analyser

  # Checks the status of a cluster object
  # @param cluster Cluster object
  def checkstatus(cluster)

    Cli::LOG.info("Checking servers...")
	
    # Check every machines
    cluster.machines.each do |machine|
		
			Cli::LOG.debug(self){"Show machine: #{machine.uid}"}


			exec = "gcloud compute instances describe #{machine.uid} --zone #{@credentials.region} --format yaml"
      Cli::LOG.debug(self){"Running command: #{exec}"}
      tmp_out = `#{exec}`
		
      # the machines is missing, so remove it
      if tmp_out.include? "not found"
        machine.state = "MISSING"
				cluster.machines.delete(machine)
      else
			# Find status in the output
			out = YAML.load(tmp_out)
			machine.state = out["status"]
      end

      # List not working machines
      if machine.state != "RUNNING"
        @notworking.push(machine)
      end
    end

    if @notworking.empty?
      Cli::LOG.info("Cluster working correctly")
    else
      @notworking.each do |machine|
        Cli::LOG.info("Machine: " + machine.uid.to_s + " error, state: " + machine.state.to_s)
      end
    end
    @notworking
  end

end
