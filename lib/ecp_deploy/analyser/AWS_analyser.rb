require_relative 'analyser'

class AWSanalyser < Analyser

  # Checks the status of a cluster object
  #
  # @param cluster Cluster object
  def checkstatus(cluster)
    Cli::LOG.debug(self){ 'Checks status on aws' }
    cluster.machines.each do |machine|
			tmp_out = `aws ec2 describe-instances --instance-ids #{machine.uid} --output text`
      Cli::LOG.debug(self){"Status: #{tmp_out}"}

      if tmp_out == ''
        status = 'MISSING'
        cluster.machines.delete(machine)
      else
        status = tmp_out.split('STATE')[1].split(" ")[1]
      end

      machine.state = status
      if status != 'running'
        @notworking.push(machine)
      end

    end

    if @notworking.empty?
      Cli::LOG.info('Cluster working correctly')
    else
      @notworking.each do |machine|
        Cli::LOG.info('Machine: '+machine.uid.to_s+' error, state: '+machine.state)
      end
    end
    @notworking
  end

end
