require_relative 'analyser'

# Like the osanalyer Class
class OSanalyser < Analyser

  # Checks the status of a cluster object
  # @param cluster Cluster object
  def checkstatus(cluster)

    Cli::LOG.info('Checking servers...')

    # Check every machines
    cluster.machines.each do |machine|
      exec = "openstack server show #{machine.uid} --os-username #{@credentials.userID} --os-project-domain-name #{@credentials.project_domain} --os-user-domain-name #{@credentials.user_domain} --os-password #{@credentials.password} --os-project-name #{@credentials.tenant} --os-auth-url #{@credentials.auth_url}"
      Cli::LOG.debug(self){"Running command: #{exec}"}
      tmp_out = `#{exec}`

      # If there is no output, the machines is missing, so remove it
      if tmp_out == ''
        machine.state = 'MISSING'
        #cluster.machines.delete(machine)
      else
        machine.state = tmp_out.firstlineoccured(' status ').split('|')[2].gsub(' ', '')
      end

      # List not working machines

      if machine.state != 'ACTIVE'
        @notworking.push(machine)
      end

    end

    if @notworking.empty?
      Cli::LOG.info('Cluster working correctly')
    else
      @notworking.each do |machine|
        Cli::LOG.info('Machine: '+machine.uid.to_s+' error, state: '+machine.state)
      end
    end
    @notworking
  end

end
