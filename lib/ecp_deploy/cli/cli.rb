#!/usr/bin/env ruby

require_relative '../parser/parser'
require_relative '../analyser/OpenStack_analyser'
require_relative '../analyser/AWS_analyser'
require_relative '../analyser/GCE_analyser'

require_relative '../provider/OpenStack_provider'
require_relative '../provider/AWS_provider'
require_relative '../provider/GCE_provider'
require_relative '../cluster/cluster'
require_relative '../cluster/machine'
require_relative '../log/multi_logger'

gem 'commander', '>= 4.4.0'
require 'commander'
require 'logger'
require 'timeout'

class ConfigReader

  attr_accessor :config_hash

  def initialize(config_file)
    @config_hash = Hash.new
    _file = File.join(File.dirname(File.expand_path(__FILE__)), config_file)
    parse_config_path _file
  end

  # recursive search for config files
  def parse_config_path(_path)
    if File.directory?(_path)
      Dir.foreach(_path) do |file|
        parse_config_path("#{_path}/#{file}") unless (file.to_s == '.' or file.to_s == '..')
      end
    else
      parse_config(_path)
    end
  end

  # parse config file into config hash
  def parse_config(_file)
    if _file.to_s.end_with?('.conf')
      Cli::LOG.debug(self) {"Found config file: #{_file}"}
      _file = File.read _file
      prefix = ''
      _file.each_line do |line|
        _line=line.chomp
        if _line[0] == '['
          prefix = _line
        else
          @config_hash[prefix + _line.split('=')[0]] = _line.split('=')[1] unless _line[0]=='#'
        end
      end
    end
  end
end

class Cli
  include Commander::Methods

  # Configure Log constants
  @log_file ='deployer.log'
  @verbose = false

  LOG=MultiLogger.new

  def initialize
    cr = ConfigReader.new '../../../config'
    @verbose = cr.config_hash['verbose'].to_boolean unless cr.config_hash['verbose'].nil?
    @log_file = cr.config_hash['deployer_log_file'] unless cr.config_hash['deployer_log_file'].nil?
    #puts _cr.config_hash
  end

  # Setup Info/Debug Log
  def inputhelper(options)
    _stdout_log = Logger.new($stdout)
    _file_log = Logger.new(@log_file)

    if options.verbose or @verbose
      _stdout_log.level = Logger::DEBUG
      _file_log.level = Logger::DEBUG
    else
      _stdout_log.level = Logger::INFO
      _file_log.level = Logger::INFO
    end

    Cli::LOG.addlogger(_file_log)
    Cli::LOG.addlogger(_stdout_log)
  end


  def self.user_input_helper(question, timeout)
    begin
      if timeout.nil?
        Cli::LOG.debug(self) {'Timeout was nil'}
        timeout = 0.001 # if 0, then wait endless
      end
      Cli::LOG.debug(self) {"parameters are: #{question} an #{timeout}"}
      Cli::LOG.info(question)
      userinput = ''
      Timeout::timeout(timeout) do
        userinput = $stdin.readline
        Cli::LOG.debug(self) {"Input was: #{userinput}"}
        userinput
      end
    rescue Timeout::Error => e
      Cli::LOG.debug(self) {"Timeout Exception: #{e.inspect}"}
      nil
    end
  end


  def create_provider(main_provider, parser)
    provider = nil
    if main_provider.downcase == 'openstack'
      provider = OpenStack.new(parser.credentialObject)
    end
    if main_provider.downcase == 'aws'
      provider = AwsProvider.new(parser.credentialObject)
    end
    if main_provider.downcase == 'gce'
      provider = GceProvider.new(parser.credentialObject)
    end
    Cli::LOG.debug(self) {"Using provider: #{provider.to_s}"}
    provider
  end


  def create_analyser(main_provider, parser)
    analyser = nil
    if main_provider.downcase == 'openstack'
      analyser = OSanalyser.new(parser.credentialObject)
    end
    if main_provider.downcase == 'aws'
      analyser = AWSanalyser.new(parser.credentialObject)
    end
    if main_provider.downcase == 'gce'
      analyser = GCEanalyser.new(parser.credentialObject)
    end
    Cli::LOG.debug(self) {"Using analyser: #{analyser.to_s}"}
    analyser
  end


  def checkFlags(credentials=false, description=false, status=false)
    raise IOError.new('Missing credential file --credentials FILE') if credentials.nil?
    raise IOError.new('Missing description file --description FILE') if description.nil?
    raise IOError.new('Missing status file --status FILE') if status.nil?
  end


  def run
    program :version, '0.0.6'
    program :description, 'ecp_deploy'


    begin

      # global options
      global_option('-c', '--credentials FILE', 'Set a credential file') do |filePath|
        @credentials = filePath
      end
      global_option('-d', '--description FILE', 'Set a description file') do |filePath|
        @description = filePath
      end
      global_option('-s', '--status FILE', 'Set a status file') do |filePath|
        @status = filePath
      end


      command :create do |c|
        global_option '--verbose'
        c.syntax = 'ecp_deploy create --credentials <credentials.json> --description <cluster-description.json> --status <statusFile.yaml>'
        c.summary = 'creates cluster'
        c.description = 'Creates the cluster described in inputfile and saves it to statusfile'
        c.example 'example:', 'ecp_deploy create --credentials data/credentials.json --description data/simpleCluster.json --status data/cluster/mycluster.yaml'
        c.action do |args, options|

          checkFlags(@credentials, @description, @status)
          current_cluster_yaml=@status

          inputhelper(options)

          parser = Parser.new(@description, @credentials)
          cluster = parser.read_cluster_json
          puts "Is a cluster: #{cluster.instance_of? Cluster}"
          provider = create_provider(cluster.getmaster.provider, parser)

          if provider == nil
            Cli::LOG.fatal(self) {"Unknown provider: #{cluster.getmaster.provider}"}
            exit(false)
          else
            provider.createCluster(cluster, current_cluster_yaml)
          end
          provider.readyCheck(cluster)
          provider.lastOrder
        end
      end

      command :check do |c|
        global_option '--verbose'
        c.syntax = 'ecp_deploy check  --credentials <credentials.json> --status <statusFile.yaml>'
        c.summary = 'checks if cluster is running'
        c.description = 'Checks if all servers of the cluster are up and running'
        c.example 'example:', 'ecp_deploy check --credentials  data/credentials.json --status  data/cluster/mycluster.yaml'
        c.action do |args, options|


          checkFlags(@credentials, @status)
          current_cluster_yaml=@status

          inputhelper(options)

          parser = Parser.new("", @credentials)
          cluster=YAML::load(File.open(current_cluster_yaml))
          analyser = create_analyser(cluster.getmaster.provider, parser)
          if analyser == nil
            Cli::LOG.fatal(self) {"Unknown provider: #{cluster.getmaster.provider}"}
            exit(false)
          else
            analyser.checkstatus(cluster)
          end
          provider.lastOrder
        end
      end

      command :update do |c|
        c.syntax = 'ecp_deploy update  --credentials <credentials.json> --description <new-cluster-description.json> --status <current-cluster.yaml>'
        c.summary = 'updates cluster'
        c.description = 'Calculates differences of inputfile and statusfile and modifies cluster accordingly'
        c.example 'example:', 'ecp_deploy update --credentials data/credentials.json --description data/simpleCluster.json --status  data/cluster/mycluster.yaml'
        c.action do |args, options|

          checkFlags(@credentials, @description, @status)

          new_cluster_path=@description
          current_cluster_path=@status
          inputhelper(options)

          Cli::LOG.info("Starting cluster update")
          parser = Parser.new(new_cluster_path, @credentials)
          current_cluster = YAML::load(File.open(current_cluster_path))
          new_cluster = parser.get_cluster
          analyser = create_analyser(new_cluster.getmaster.provider, parser)
          provider = create_provider(new_cluster.getmaster.provider, parser)

          if analyser != nil && provider != nil
            analyser.registerprovider(provider)
            cluster = analyser.compare(current_cluster, new_cluster, parser.cluster_was_yaml)
            provider.savecluster(cluster, current_cluster_path)
            provider.readyCheck(cluster)
            provider.lastOrder
          else
            Cli::LOG.fatal(self) {"Unknown provider: #{new_cluster.getmaster.provider}"}
            exit(false)

          end
        end
      end


      command :teardown do |c|
        global_option '--verbose'
        c.syntax = 'Cli teardown --credentials <credentials.json> --status <statusFile.yaml>'
        c.summary = 'Shuts down cluster'
        c.description = 'Terminates all servers of the cluster'
        c.example 'example:', 'ecp_deploy teardown --credentials data/credentials.json --status data/cluster/mycluster.yaml'
        c.action do |args, options|

          checkFlags(@credentials, @status)

          credential_file=@credentials
          current_cluster_yaml=@status
          inputhelper(options)

          Cli::LOG.info("Shutting down cluster")

          parser = Parser.new("", credential_file)
          current_cluster = YAML::load(File.open(current_cluster_yaml))
          provider = create_provider(current_cluster.getmaster.provider, parser)

          if provider == nil
            Cli::LOG.error(self) {"Unknown provider: #{current_cluster.getmaster.provider}"}
          else
            provider.deletecluster(current_cluster, current_cluster_yaml)
          end
          provider.lastOrder
        end
      end

      command :debug do |c|
        global_option '--verbose'
        c.action do |args, options|
          inputhelper(options)
          checkFlags(@credentials, @status)

          parser = Parser.new("", @credentials)
          current_cluster = YAML::load(File.open(@status))
          provider = create_provider(current_cluster.getmaster.provider, parser)

          c = current_cluster
          c.set_initialized_by_ip('10.50.10.119')
          puts c
          c.set_initialized_by_ip('10.50.10.119',false)
          puts c
          #$stdout.puts(Cli::user_input_helper('Bitte innerhalb von 5 Sekunden eine Eingabe tätigen!', 5))
          # Debug command for testing only
        end
      end
    rescue IOError => e
      Cli::LOG.fatal(self) {"Input error: #{e.inspect}"}
    end
    run!
  end
end
