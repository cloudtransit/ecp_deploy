# Load data structures for external use
require_relative '../lib/ecp_deploy/cluster/cluster'
require_relative '../lib/ecp_deploy/cluster/machine'

## Helper function for string ##
class String
  def firstlineoccured(pattern)
    self.each_line do |line|
      return line if (line.include?(pattern))
    end
  end

  def whitespacetoalllines(num)
    tmp_data = ""
    self.lines.each do |line|
      tmp_data+=line.addwhitespace(num)
    end
    tmp_data
  end

  def addwhitespace(num)
    ret = ""
    num.times do
      ret += " "
    end
    ret.concat(self)
  end

  def to_boolean
    self.downcase == 'true'
  end
end
