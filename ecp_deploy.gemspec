Gem::Specification.new do |spec|
  spec.name        = 'ecp_deploy'
  spec.version     = '0.0.6'
	spec.authors     = ["Nane Kratzke", "Peter-Christian Quint", "Christian-Friedrich Stüben", "Arne Jan Salveter"]
  spec.email       = 'nane.kratzke@fh-luebeck.de', 'peter-christian.quint@fh-luebeck.de' 'christian-friedrich.stueben@stud.fh-luebeck.de', 'arne-jan.salveter@stud.fh-luebeck.de'


	spec.summary     = "A tool to manage container-cluster including scheduling and orchestration."
  spec.description = "It considers raising, scale and teardown clusters over different providers."
 	spec.homepage    = 'https://cosa.fh-luebeck.de/'
  spec.license     = 'MIT'
	spec.executables = ['ecp_deploy']


  spec.files       = [
 "lib/ecp_deploy.rb",
 "lib/ecp_deploy/analyser/analyser.rb",
 "lib/ecp_deploy/analyser/AWS_analyser.rb",
 "lib/ecp_deploy/analyser/OpenStack_analyser.rb",
"lib/ecp_deploy/analyser/GCE_analyser.rb",
 "lib/ecp_deploy/cli/cli.rb",
 "lib/ecp_deploy/cluster/cluster.rb",
 "lib/ecp_deploy/cluster/machine.rb",
 "lib/ecp_deploy/credentials/credentials.rb",
 "lib/ecp_deploy/credentials/AWS_credentials.rb",
 "lib/ecp_deploy/credentials/OpenStack_credentials.rb",
 "lib/ecp_deploy/credentials/GCE_credentials.rb",
 "lib/ecp_deploy/parser/parser.rb",
 "lib/ecp_deploy/provider/AWS_provider.rb",
 "lib/ecp_deploy/provider/provider.rb",
 "lib/ecp_deploy/provider/OpenStack_provider.rb",
 "lib/ecp_deploy/storage/storage.rb",
 "lib/ecp_deploy/storage/cephfs_storage.rb",
 "lib/ecp_deploy/storage/glusterfs_storage.rb",
 "lib/ecp_deploy/storage/flocker_storage.rb",
 "lib/ecp_deploy/provider/GCE_provider.rb",
 "lib/ecp_deploy/templates/master_template.yaml",
 "lib/ecp_deploy/templates/slave_template.yaml",
 "lib/ecp_deploy/templates/cephfs_storage_template.yaml",
 "lib/ecp_deploy/templates/glusterfs_storage_template.yaml",
 "lib/ecp_deploy/templates/flocker_storage_template.yaml",
 "lib/ecp_deploy/templates/securityGroup.json",
 "lib/ecp_deploy/templates/master_update_template.yaml",
 "lib/ecp_deploy/log/multi_logger.rb",
 "config/etc/log.conf",
 "config/platform/platform.conf",
 "config/provider/provider.conf"
]
 

# Hier evt. noch die development_dependency
	spec.add_runtime_dependency 'openstack', '~> 2.0', '>= 2.0.2'
	spec.add_runtime_dependency 'commander', '~> 4.0', '>= 4.4.0'
	spec.add_runtime_dependency 'hashdiff', '~> 0', '>= 0.3.0'
	spec.add_runtime_dependency 'core_ext', '~> 0', '>= 0.0.6'
	spec.add_runtime_dependency 'json', '~> 1.8', '>= 1.8.3'
	spec.add_runtime_dependency 'rubysl-securerandom', '~> 2.0', '>= 1.5.0'


end
