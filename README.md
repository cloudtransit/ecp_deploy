# ecp_deploy

[*Version 0.0.5*](https://bitbucket.org/cloudtransit/ecp_deploy/wiki/Versionen)

**Warning: Not for production use**

**Currently this release is for developing, testing and research only. Please read the following instructions carefully.**

A tool to manage container-cluster including scheduling and orchestration.
It considers

* raising
* scale and 
* teardown 

clusters over different providers (like OpenStack, Amazon aso.) via comandline and a simple description format.

The current version is only working for **OpenStack-Mitaka** and **AWS-C2**. As cluster software only Kubernets is used (Later release will integrate more provider- and cluster support).

## Installation 

### Dependencies
There are some dependencies to install, before the deployer is correctly running.

## AWS
In this version is only one combination of CoreOs-comunity-image ("ami-0e1dfe61") and region ("eu-central-1") testet.
When using another region please search for a "CoreOS 1010.1.0" or greater (comunity-) AMI and test it carefully.

## OpenStack
The current solution requires a **flat network** configuration.
The cluster-used security rules has to be specified. When teardown the cluster (with teardown command), also the OpenStack security groups will be deleted automatically  . 

To run the deployer for a new provider you must install the specific extensions:
```
#!bash
gem install openstack

```

## Mainprogramm
The dependences for the core are (only the gem without any provider like OpenStack or AWS):

* Ruby
* kubectl
* Python
* other gems:  core_ext, hashdiff, commander


## Install-Script
An easier way to install all dependences is to use the following script.

Use this to check the options:
```
#!bash

your-script-name -h
```


```
#!/bin/bash
# Use this script to set up an envirement for the 'ecp_deploy'.

## Parameter ##
# select kubectl version
kubctlVersion='v1.3.1'

# select ruby version   
rubyVersion='2.3.1'

## Funktions ##
function prepInstance  {
 echo -e '### prepare instance ###\n'
 hostName=`head -n 1 /etc/hostname`
 var=" $hostName"
 sed -i '1 s#$#'"${var}#" /etc/hosts
 chown ubuntu /etc/hostname
 echo deployer >> /etc/hostname
 chown ubuntu /etc/hosts
 echo 127.0.0.1 deployer >> /etc/hosts 
 touch /home/ubuntu/.ssh/config
 chmod 644 /home/ubuntu/.ssh/config
 chown ubuntu /home/ubuntu/.ssh/config
}

function installCubctl  {
 echo -e '\n### Install cubctl ###\n'
 curl -O https://storage.googleapis.com/kubernetes-release/release/v1.5.4/bin/linux/amd64/kubectl
 sudo cp kubectl /usr/local/bin/kubectl
 sudo chmod +x /usr/local/bin/kubectl
 #ready-check
 kubectl cluster-info
 rm kubectl
}

function installRuby  {
 echo -e '\n### Install Ruby ###\n'
 # add key
 gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
 # install Ruby Version Manager (RVM)
 \curl -sSL https://get.rvm.io | bash
 # activate rvm
 #source /home/ubuntu/.rvm/scripts/rvm
 # install ruby
 rvm install $rubyVersion
}

function installOpenStack  {
 echo -e '\n## Install OpenStack ##\n'
 apt-get install -y python-openstackclient
}

function installCephDeploy  {
 echo -e '\n## Install ceph-deploy ##\n'
 sudo pip install ceph-deploy
}

function installDocker  {
 echo -e '\n## Install Docker ##\n'
 sudo apt install docker.io -y
 sudo curl -sSL https://raw.githubusercontent.com/ClusterHQ/unofficial-flocker-tools/master/go.sh | sh
}

function installGems  {
 echo -e '\n## Install gems ##\n'
 apt-get install -y rubygems 
 gem install core_ext hashdiff commander openstack
}

function installPython  {
 echo -e '\n### Install Python ###\n'
 # necessary: Python 2.6.5+ or Python 3 version 3.3+
 apt-get install -y python
}

function installPip  {
 echo -e '\n## Install pip ##\n'
 apt-get update -y
 apt-get install -y python-pip
 export LC_ALL=C 
 pip install --upgrade pip
}

function installAws  {
 echo -e '\n### Install AWS-Cli ###\n'
 #necessary workaround for pip xmltodict
 export LC_ALL=C
 pip install awscli
 aws --version 
}

function addController  {
 echo -e '\n### Install Controler ###\n'
 sed -i '1i ' /etc/hosts
}

function installGoogleComputeEngine  {
 export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
 echo "deb https://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
 curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
 apt-get update && sudo apt-get install -y google-cloud-sdk
}

## main-script ##
while test $# -gt 0; do
 case "$1" in
  -h|--help)
   echo '---------------------------------------------------'
   echo -e 'How to use the flags:\n'
   echo -e '   -p :    prepare an Ubuntu 16.04 instance'
   echo -e '   -a :    Install all necessary components.'
   echo -e '   -c :    Install cubctl.'
   echo -e '   -cd :   Install ceph-deploy.'
   echo -e '   -d :    Install Docker.'
   echo -e '   -r :    Install ruby.'
   echo -e '   -o :    Install OpenStack.'
   echo -e '   -g :    Install ruby-gems.'
   echo -e '   -py :   Install python.'
   echo -e '   -pip :  Install pip.'
   echo -e '   -aws :  Install AWS-cli.\n\n'
   echo -e 'Examples:'
   echo -e 'Install prepare an ubuntu 16.04 instance and install all necessary components:'
   echo -e 'sudo bash prepareInstance.sh -p -a\n'
   echo -e 'Install cubctl and AWS-cli:'
   echo -e 'sudo bash prepareInstance.sh -c -aws\n'
   echo '---------------------------------------------------'
   exit 0
    ;;

	-p)
   shift
   prepInstance
  ;;

  -a|--all)
   shift
   installCubctl
   installCephDeploy
   installDocker 
   installRuby 
   installGems 
   installPython 
   installPip 
   installAws
   installOpenStack
   installGoogleComputeEngine
  exit 0
  ;;

  -c)
   shift
   installCubctl
  ;;
  
  -cd)
   shift
   installCephDeploy
  ;;
  
  -d)
   shift
   installDocker
  ;;
      
 -o)
   shift
   installOpenStack 
  ;;

  -r)
   shift
   installRuby 
  ;;

  -g)
   shift
   installGems 
  ;;
    
  -py)
   shift
   installPython 
  ;;

  -pip)
   shift
   installPip 
  ;;

  -aws)
   shift
   installAws 
  ;;

  -gce)
   shift
   installGoogleComputeEngine 
  ;;

  *)
   break
  ;;
  esac
done
echo -e '\ndone\n'
exit 0
```


## Usage
How to use the tool.


### Start a cluster
There are *three* parameters to start a cluster:

```
#!RUBY
$ ruby ecp_deploy create --credentials <credentials-file>.json  --description <cluster-file>.json --status <status-file>.yaml
```
*Example configuration:* See "config" directory for example credential and cluster files for your chosen provider. The status file will be created if it not exist.

After creation finished you can use 
```
kubectl get nodes
```
to check your cluster. Note: It can take a few minutes till cluster is available.

### Status-Check
Check the current status of your cluster.

```
#!ruby
$ ruby ecp_deploy check --credentials <credentials-file>.json --status <status-file>.yaml
```

### Scale-Up
Scale the cluster up or down.

```
#!ruby
$ ruby ecp_deploy.rb update --credentials <credentials-file>.json --description <cluster-file>.json  --statu <status-file>.yaml

```

### teardown
Theardown the cluster and delete automaticly all security-groups.

```
#!ruby
$ ruby ecp_deploy teardown --credential <credentials-file>.json --statu <status-file>.yaml

```


### Further commands

There are further commands to

- hybride clusters
- multy-master cluster
- general cluster description.

You get usage instructions via:

    $ ecp_deploy --help

## Read the Wiki

The deployer-Wiki is provided via Bitbucket as a public git repository. 
So you can get all the infos via the following git command.

    $  https://bitbucket.org/cloudtransit/deployer/wiki/Home
   
## Changelog

## Acknowledgment
This tool is developed within the scope of the research project Cloud TRANSIT, funded by German Federal Ministry of Education
and Research (03FH021PX4), by the CoSA Center of Excellence, University of Applied Sciences Luebeck

*Prof. Dr. rer. nat. Dipl.-Inform.* **Nane Kratzke** and *Dipl.-Inf.* **Peter-Christian Quint**
The student-authors are Christian-Friedrich Stueben and Arne Salveter.

For more informations please visit https://cosa.fh-luebeck.de/cloud_transit




